/**
  ******************************************************************************
  * @author  Tomasz Lahus
  * @version 0.98
  * @date    21-01-2016
  * @brief   
	*				21-01-2015	-	First release
	
	Obsluga opiera sie na wprowadzaniu przez przerwania od Exti flag do tablicy ststusowej "BtnFlag_table"
	i usuwaniu tych flag po wykonaniu sie funkcji ButtonFunc_Exe() umieszczonej w programie glownym. 
	Zawiera ona wskazniki na funkcje przypisane do danego przycisku. 
	Wskazniki mozna zmieniac poprzez SetButtonsFunc(). 
	Jej argumentem jest wyswietlany obecnie ekran, poniewaz przyciski zazwyczaj zmieniaja
	swoja funkcjionalnosc po przejsciu do innego ekranu.
	Dostepne flagi to aktualnie przycisniety, kr�tkie przycisniecie, dlugie przycisniecie (define LONG_PRESS_CNT w ms).
	Tablice statusowa trzeba czyscic po zmianie ekranu wyswietlanego (zmianie funkcjonalnosci przyciskow).
	* 			
  ******************************************************************************
**/ 

#include "buttons.h"
#include "lcdst7565r_unicode.h"
#include "memory.h"
#include "arm_math.h"
//#include "main.h"
//#include "rtc.h"
#include "system.h"
#include "display.h"
#include "filters.h"
#include "dfsdm.h"
#include "iwdg.h"
//wskaznik na funkcje wyswietlanego obecnie ekranu, umieszczony w mainie.

extern float PlayBuff_float[2048];
volatile uint8_t BtnFlag_table[NUM_OF_BTNS];
volatile uint16_t press_cnt_tab[NUM_OF_BTNS];

volatile uint8_t testlong;     /*zmienna konieczna do poprawnego działania przycików*/
uint32_t time_in_seconds;
uint8_t days;

uint16_t free_space_results;
uint16_t free_space_backlit;

//put reference to this func in every EXTI interrupt - rising, falling edge
void Button_IrqEvent(void){
	_delay_ms(40);

	if(EXTI->PR1 & BTN_LEFT_PIN){
	
		if(BTN_LEFT_PRESSED)
 			BtnFlag_table[BTN_LEFT_INDEX] = IS_PRESSED_FLAG ;
		else if(BtnFlag_table[BTN_LEFT_INDEX] & IS_PRESSED_FLAG){
			if(!(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)){
				if (testlong==0) BtnFlag_table[BTN_LEFT_INDEX] |=  SHORT_PRESS_FLAG;
				ResetTimeOff();
			}
			testlong=0;
			BtnFlag_table[BTN_LEFT_INDEX] &= ~IS_PRESSED_FLAG;
			press_cnt_tab[BTN_LEFT_INDEX]=0;
		}
		EXTI->PR1 = BTN_LEFT_PIN;
	}
	if(EXTI->PR1 & BTN_RIGHT_PIN){
		if(BTN_RIGHT_PRESSED)
			BtnFlag_table[BTN_RIGHT_INDEX] = IS_PRESSED_FLAG;
		else if(BtnFlag_table[BTN_RIGHT_INDEX] & IS_PRESSED_FLAG){
			if(!(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)){
				if (testlong==0) 	BtnFlag_table[BTN_RIGHT_INDEX] |=  SHORT_PRESS_FLAG;
				ResetTimeOff();
			}
			testlong=0;
			BtnFlag_table[BTN_RIGHT_INDEX] &= ~IS_PRESSED_FLAG;
			press_cnt_tab[BTN_RIGHT_INDEX]=0;
		}
		EXTI->PR1 = BTN_RIGHT_PIN;
	}
	
	if(EXTI->PR1 & BTN_DOWN_PIN){
		if(BTN_DOWN_PRESSED)
			BtnFlag_table[BTN_DOWN_INDEX] = IS_PRESSED_FLAG;
		else if(BtnFlag_table[BTN_DOWN_INDEX] & IS_PRESSED_FLAG){
			if(!(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)){
				if (testlong==0) 	BtnFlag_table[BTN_DOWN_INDEX] |=  SHORT_PRESS_FLAG;
				ResetTimeOff();
			}
			testlong=0;
			BtnFlag_table[BTN_DOWN_INDEX] &= ~IS_PRESSED_FLAG;
			press_cnt_tab[BTN_DOWN_INDEX]=0;
		}
		EXTI->PR1 = BTN_DOWN_PIN;
	}
	
	if(EXTI->PR1 & BTN_UP_PIN){
		if(BTN_UP_PRESSED)
			BtnFlag_table[BTN_UP_INDEX] = IS_PRESSED_FLAG;
		else if(BtnFlag_table[BTN_UP_INDEX] & IS_PRESSED_FLAG){
			if(!(BtnFlag_table[BTN_UP_INDEX] & LONG_PRESS_FLAG))
				BtnFlag_table[BTN_UP_INDEX] |=  SHORT_PRESS_FLAG;
			BtnFlag_table[BTN_UP_INDEX] &= ~IS_PRESSED_FLAG;
			press_cnt_tab[BTN_UP_INDEX]=0;
		}
		EXTI->PR1 = BTN_UP_PIN;
	}
	
}
//put reference to this func in systick interrupt - 1ms conf
void Button_LongPress(void){
	
	if(BtnFlag_table[BTN_LEFT_INDEX] & IS_PRESSED_FLAG){
		press_cnt_tab[BTN_LEFT_INDEX]++;
		if(press_cnt_tab[BTN_LEFT_INDEX] == LONG_PRESS_CNT ){
			BtnFlag_table[BTN_LEFT_INDEX] &=  ~SHORT_PRESS_FLAG;
			BtnFlag_table[BTN_LEFT_INDEX] |=  LONG_PRESS_FLAG;
			testlong++;
		}
	}
	
	if(BtnFlag_table[BTN_RIGHT_INDEX] & IS_PRESSED_FLAG){
		press_cnt_tab[BTN_RIGHT_INDEX]++;
		if((press_cnt_tab[BTN_RIGHT_INDEX] == LONG_PRESS_CNT) && BTN_RIGHT_PRESSED){
			BtnFlag_table[BTN_RIGHT_INDEX] &=  ~SHORT_PRESS_FLAG;
			BtnFlag_table[BTN_RIGHT_INDEX] |=  LONG_PRESS_FLAG;
			testlong++;
		}	
	}
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & IS_PRESSED_FLAG){
		press_cnt_tab[BTN_DOWN_INDEX]++;
		if(press_cnt_tab[BTN_DOWN_INDEX] == LONG_PRESS_CNT ){
			BtnFlag_table[BTN_DOWN_INDEX] &=  ~SHORT_PRESS_FLAG;
			BtnFlag_table[BTN_DOWN_INDEX] |=  LONG_PRESS_FLAG;
			testlong++;
		}
	}
	
	if(BtnFlag_table[BTN_UP_INDEX] & IS_PRESSED_FLAG){
		press_cnt_tab[BTN_UP_INDEX]++;
		if((press_cnt_tab[BTN_UP_INDEX] == LONG_PRESS_CNT) && BTN_UP_PRESSED){
			BtnFlag_table[BTN_UP_INDEX] &=  ~SHORT_PRESS_FLAG;
			BtnFlag_table[BTN_UP_INDEX] |=  LONG_PRESS_FLAG;
		}	
	}
}

//pointer on btn func that is already active
void (*BTN_L_func)(void);
void (*BTN_R_func)(void);
void (*BTN_U_func)(void);
void (*BTN_D_func)(void);

//put reference to this func in main body
void ButtonFunc_Exe(void){
	
	if(!(BtnFlag_table[BTN_LEFT_INDEX] ==0))
		BTN_L_func();
	if(!(BtnFlag_table[BTN_RIGHT_INDEX] ==0))
		BTN_R_func();
	if(!(BtnFlag_table[BTN_UP_INDEX] ==0))
		BTN_U_func();
	if(!(BtnFlag_table[BTN_DOWN_INDEX] ==0))
		BTN_D_func();

	for(uint8_t i=0;i<NUM_OF_BTNS;i++)
		BtnFlag_table[i]&=IS_PRESSED_FLAG;
	
	
//	if (menuposition==3 || menuposition==1 || menuposition==5)
	if (menuposition==3 ||  menuposition==5)
	{
		if(BTN_DOWN_UNPRESSED) testlong=0;
	}
}



void SetButtonsFunc(uint8_t screen){
	switch(screen)
	{
		case 0:
			BTN_L_func = BL_MenuScreen;
			BTN_R_func = BR_MenuScreen;
			BTN_D_func = BD_MenuScreen;
		break;
		case 1:
			BTN_L_func = BL_AverageScreen;
			BTN_R_func = BR_AverageScreen;
		  BTN_D_func = BD_AverageScreen;
		break;
		case 2:
			BTN_L_func = BL_ResetScreen;
			BTN_R_func = BR_ResetScreen;
			BTN_D_func = BD_ResetScreen;
		break;
		case 3:
			BTN_L_func = BL_OffScreen;
			BTN_R_func = BR_OffScreen;
			BTN_D_func = BD_OffScreen;
		break;
		case 4:
			BTN_L_func = BL_SetTimeScreen;
			BTN_R_func = BR_SetTimeScreen;
			BTN_D_func = BD_SetTimeScreen;
		break;
		case 5:
			BTN_L_func = BL_MainOffScreen;
			BTN_R_func = BR_MainOffScreen;
			BTN_D_func = BD_MainOffScreen;
		break;
		case 6:
			BTN_L_func = BL_CalibrationScreen;
			BTN_R_func = BR_CalibrationScreen;
			BTN_D_func = BD_CalibrationScreen;
		break;
		case 7:
			BTN_L_func = BL_MenuPos1;		//LABEL SET AVERAGE TIME
			BTN_R_func = BR_MenuPos1;
			BTN_D_func = BD_MenuPos1;
		break;
		case 8:
			BTN_L_func = BL_MenuPos2;		//LABEL display time
			BTN_R_func = BR_MenuPos2;
			BTN_D_func = BD_MenuPos2;
		break;
		case 9:
			BTN_L_func = BL_MenuPos3;		//LABEL backlit
			BTN_R_func = BR_MenuPos3;
			BTN_D_func = BD_MenuPos3;
		break;
		case 10:
			BTN_L_func = BL_MenuPos4;		//LABEL lang
			BTN_R_func = BR_MenuPos4;
			BTN_D_func = BD_MenuPos4;
		break;
		case 11:
			BTN_L_func = BL_MenuPos5;		//LABEL calibr
			BTN_R_func = BR_MenuPos5;
			BTN_D_func = BD_MenuPos5;
		break;
		case 12:
			BTN_L_func = BL_MenuPos6;		//LABEL sys info
			BTN_R_func = BR_MenuPos6;
			BTN_D_func = BD_MenuPos6;
		break;
		case 13:
			BTN_L_func = BL_MenuPos7;		//LABEL default
			BTN_R_func = BR_MenuPos7;
			BTN_D_func = BD_MenuPos7;
		break;
		case 14:
			BTN_L_func = BL_MenuPos8;		//LABEL EXIT
			BTN_R_func = BR_MenuPos8;
			BTN_D_func = BD_MenuPos8;
		break;	
		case 15:
			BTN_L_func = BL_MenuBacklit;		//MENU SET BACKLIT
			BTN_R_func = BR_MenuBacklit;
			BTN_D_func = BD_MenuBacklit;
		break;
		case 16:
			BTN_L_func = BL_MenuSetTimeAverage;		//MENU SET TIME AVERAGE
			BTN_R_func = BR_MenuSetTimeAverage;
			BTN_D_func = BD_MenuSetTimeAverage;
		break;
		case 17:
			BTN_L_func = BL_MenuDisplayResults;		//MENU display results
			BTN_R_func = BR_MenuDisplayResults;
			BTN_D_func = BD_MenuDisplayResults;
		break;
		case 18:
			BTN_L_func = BL_MenuSetLanguage;		//MENU display results
			BTN_R_func = BR_MenuSetLanguage;
			BTN_D_func = BD_MenuSetLanguage;
		break;
		case 19:
			BTN_L_func = BL_MenuUserCalibration;		//MENU user calibration
			BTN_R_func = BR_MenuUserCalibration;
			BTN_D_func = BD_MenuUserCalibration;
		break;
		case 20:
			BTN_L_func = BL_MenuSystemInfo;		//MENU system info
			BTN_R_func = BR_MenuSystemInfo;
			BTN_D_func = BD_MenuSystemInfo;
		break;
		case 21:
			BTN_L_func = BL_MenuDefaultSettings;		//MENU default settings
			BTN_R_func = BR_MenuDefaultSettings;
			BTN_D_func = BD_MenuDefaultSettings;
		break;
		case 22:
			BTN_L_func = BL_MenuWarningFilters;		//MENU default settings
			BTN_R_func = BR_MenuWarningFilters;
			BTN_D_func = BD_MenuWarningFilters;
		break;
		case 23:
			BTN_L_func = BL_MenuSelectCalibration;		//MENU select calibration
			BTN_R_func = BR_MenuSelectCalibration;
			BTN_D_func = BD_MenuSelectCalibration;
		break;
		case 24:
			BTN_L_func = BL_BacklightOff;		//MENU select calibration
			BTN_R_func = BR_BacklightOff;
			BTN_D_func = BD_BacklightOff;
		break;
		
		
		
		
		
//etc..
	}	
}



void BL_AverageScreen(void){ //menuposiotion 1
	
	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		if(TIM1->CCR4)			//if backlight is on
		{
			menuposition=0;						/*		return to main menu		*/
			parameters &= ~(0x10); 		/*		reset flag display time average		*/
			TIM7->CNT = 10000;
		}
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	{
		if(TIM1->CCR4)
		{
			menuposition=0;						/*		return to main menu		*/
			parameters &= ~(0x10); 		/*		reset flag display time average		*/
			TIM7->CNT = 10000;
		}			
	}
}

void BR_AverageScreen(void){//menuposiotion 1
		
		char a[5];
		char b[5];
		char c[5];
		uint8_t len;

	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		if(TIM1->CCR4)			//if backlight is on
		{	
			if (DISPLAY_TIME_AVERAGE)		 
			{
				free_space_results= CheckFreeSpace(page_end_wsk,1024);
				if (free_space_results <=30) 
				{
					erase_page_bank(ERASE_PAGE_DATA_1);
					CopyLastTenResults(page_end_wsk_double,beginlistspace);
					erase_page_bank(ERASE_PAGE_DATA_2);
				}
				save_data_to_flash(page_end_wsk,(float)artm_db_result_inf,GetSecondsRtc(),1024);
				clear_lcd();
				len=getStringLength_pixels_unicode((void *)language_labels[language][13],&cambria9pt);
				displayText_unicode((128-len)/2,22,(void *)language_labels[language][13],&cambria9pt,1);
				lcd_updatewholedisplay(0);
				HAL_Delay(100);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(100);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(100);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(150);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(150);
				HAL_IWDG_Refresh(&hiwdg);
				
				
				battery_check(menuposition, 1);
			}
		}
	}

	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)//menuposiotion 1
	{
		if(TIM1->CCR4)			//if backlight is on
		{
			clear_lcd();
			sprintf(a,"%02d",hour_time_aver);
			sprintf(b,"%02d",min_time_aver);
			sprintf(c,"%02d",sec_time_aver);
			displayText_unicode(2,12,a,&calibri26pt,1);
			displayText_unicode(39,14,":",&calibri26pt,1);
			displayText_unicode(46,12,b,&calibri26pt,1);
			displayText_unicode(83,14,":",&calibri26pt,1);
			displayText_unicode(90,12,c,&calibri26pt,1);
			displayText_unicode(12,2,"hh",&cambria9pt,1);
			displayText_unicode(52,2,"mm",&cambria9pt,1);	
			displayText_unicode(103,2,"ss",&cambria9pt,1);	
		
			rectangle (1,52,11,28,0);
			len=getStringLength_pixels_unicode((void *)language_labels[language][9],&cambria_9ptA);		//SET AVERAGE TIME9
			displayText_unicode((128-len)/2,50,(void *)language_labels[language][9],&cambria_9ptA,1);//128-len
					
			lcd_updatewholedisplay(0);
			HAL_Delay(200);
			HAL_IWDG_Refresh(&hiwdg);
			HAL_Delay(200);
			HAL_IWDG_Refresh(&hiwdg);
			HAL_Delay(200);
			HAL_IWDG_Refresh(&hiwdg);
			HAL_Delay(200);
			HAL_IWDG_Refresh(&hiwdg);
			HAL_Delay(200);
			HAL_IWDG_Refresh(&hiwdg);				
			clear_lcd();
			battery_check(menuposition, 1);
			TIM7->CNT = 10000;
		}
	}
}

void BD_AverageScreen(void){//menuposiotion 1
		

	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	{
		if(TIM1->CCR4)			//if backlight is on
		{
			if(AVERAGING_IS_ACTIVE)
			{
				menuposition=2;
				TIM7->CNT = 10000;	
			}
			else  // po włączeniu miernika, czas 00:00:00,  usrednianie nieaktywne
			{
				if(!(AVERAGE_TIME_REACHED))   //na
				{
					if (!(DISPLAY_TIME_AVERAGE))    // po włączeniu miernika lub zatrzymaniu uśredniania
					{
						ResetRtc();

						sumsec_time_aver=hour_time_aver*3600+min_time_aver*60+sec_time_aver;

						parameters |= 0x0008;				//wyszyść min/max average na dole wyświetlacza  CLEAR_MIN_MAX_AVERAGE is active
						parameters &= ~(0x0040);		//zablokuj zmienną artm db result, WAIT_UNTIL_CLR_AVERAGE is active
						parameters |= 0x2000;
						rectangle (90,52,8,1,1);
						rectangle (91,53,6,1,1);
						rectangle (92,54,4,1,1);
						rectangle (93,55,2,1,1);
						lcd_updatewholedisplay(0);
					}
					parameters |= 0x0800;				
					TIM7->CNT = 10000;
				}
				else //po zakończeniu pomiaru wyzeruj wskazania
				{
					menuposition=1;
					battery_check(menuposition, 1);
					parameters &= ~(0x0800);
					HAL_Delay(100);
					parameters &= ~(0x1000);
					parameters &= ~(0x2000);
					time_to_off=0;	
				}
			}
		}	
	}

	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	{
		if(TIM1->CCR4){
			if(AVERAGING_IS_ACTIVE)
			{
				menuposition=3;
				TIM7->CNT = 10000;
			} else AutoTurnOff(1,0);
		}	
	}
}

void BR_ResetScreen(void){
	
	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		if(TIM1->CCR4)			//if backlight is on
		{	
			menuposition=1;
			battery_check(menuposition, 1);
			ResetRtc();
			parameters &= ~(0x0800);
			TIM7->CNT = 10000;
			HAL_Delay(100);
			parameters &= ~(0x1000);
			parameters &= ~(0x2000);
			time_to_off=0;
			battery_check(menuposition, 1);
			//TIM7->CNT = 10000;
			
		}
	}
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BL_ResetScreen(void){
	
	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		if(TIM1->CCR4)			//if backlight is on	
		{
			menuposition=1;
			TIM7->CNT = 10000;
			battery_check(menuposition, 1);
		}
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_ResetScreen(void){
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	;
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	;
}
//************************OFF SCREEN*********************

void BR_OffScreen(void){   //average screen 
	
	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		if(TIM1->CCR4)			//if backlight is on
    AutoTurnOff(1,0);
	}
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}



void BL_OffScreen(void){
	
	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		if(TIM1->CCR4)			//if backlight is on
		{
			clear_lcd();
			menuposition=1;
			battery_check(menuposition, 1);
			TIM7->CNT = 10000;
			
		}
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_OffScreen(void){
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	;
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	;
}

//******************Calibration Screen********************
void BR_CalibrationScreen(void){
	
	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		if(TIM1->CCR4)			//if backlight is on
		{	
			uint32_t ind;
			HAL_Delay(200);
			calibration_value2=rmsa4;
			//calibration_value2 = Integral_Trap(PlayBuff_float, 48000.f, 2048);
			//arm_max_f32(PlayBuff_float, 2048,&calibration_value2, &ind);
			//calibration_value2 = fabsf(calibration_value2);
		}
	}	
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}



void BL_CalibrationScreen(void){
	
	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		if(TIM1->CCR4)			//if backlight is on
		{
			save_data_to_flash(page_factory_calib,calibration_value2,10,512);	
			clear_lcd();
			menuposition=0;
			battery_check(menuposition, 1);	
		}
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_CalibrationScreen(void){
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	;
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	{
		if(TIM1->CCR4)			//if backlight is on
		{
			clear_lcd();
			lcd_updatewholedisplay(0);
			_delay_ms(100);
			displayText_unicode(40,8,power_icon,&icon,1);
			lcd_updatewholedisplay(0);
	  
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);

			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
		}
	}
}


//*******************MAIN OFF SCREEN**********************
void BR_MainOffScreen(void){
	
	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		if(TIM1->CCR4)			//if backlight is on
		AutoTurnOff(1,0);
	}
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}



void BL_MainOffScreen(void){
	
	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		if(TIM1->CCR4)			//if backlight is on
		{
			menuposition=0;
			battery_check(menuposition, 1);
			TIM7->CNT = 10000;
		}
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_MainOffScreen(void){
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	;
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	;
}


//*********************SET TIME SCREEN***********  case_4
void BR_SetTimeScreen(void){
	
	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		if(TIM1->CCR4)			//if backlight is on
		{
			clear_lcd();
			menuposition=1;
			battery_check(menuposition, 1);
		}			
	}
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BL_SetTimeScreen(void){
	
	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	;
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_SetTimeScreen(void){

	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	;
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	;
}


void BR_MenuScreen(void){
	
	
	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		if(TIM1->CCR4)			//if backlight is on
		{
			parameters ^= 0x0002;
			TIM7->CNT = 10000;
		}
	}
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	{
		if(TIM1->CCR4)			//if backlight is on
		{
			if (!(DISPLAY_TIME_AVERAGE))    //if parameters 0x0800 ==1
			{
				HAL_DFSDM_FilterRegularStop_DMA(&hdfsdm1_filter0);
				parameters &= ~(0x0002);
				menuposition=7;
				TIM7->CNT = 10000;
			}else
				{ 
				clear_lcd();
				menuposition=22;
				TIM7->CNT = 10000;	
				}
		}
	}
	
}
void BL_MenuScreen(void){
	
	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		if(TIM1->CCR4)			//if backlight is on
		{
			if (!(DISPLAY_TIME_AVERAGE))    //if parameters 0x0800 ==1
			{
				parameters ^= 0x0001;
				TIM7->CNT = 10000;
			}else
				{ 
				menuposition=1;
				parameters |= 0x10;
				parameters &= ~(0x0001);  // a curve
				parameters &= ~(0x0002); //fast	
				TIM7->CNT = 10000;
				}
		}
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	{
		if(TIM1->CCR4)			//if backlight is on
		{
			menuposition=1;
			parameters |= 0x10;
			parameters &= ~(0x0001);
			parameters &= ~(0x0002); //fast	
			TIM7->CNT = 10000;
		}
	}
}



void BU_MenuScreen(void){
	if(BtnFlag_table[BTN_UP_INDEX] & SHORT_PRESS_FLAG)
		;
	if(BtnFlag_table[BTN_UP_INDEX] & LONG_PRESS_FLAG)
		;//Func();
}
void BD_MenuScreen(void){
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	{
		
		if(TIM1->CCR4){
		parameters |= 0x04;
		TIM7->CNT = 10000;
		}
	}
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	{
		if(TIM1->CCR4){
			if(AVERAGING_IS_ACTIVE)
			{
				menuposition=5;
				TIM7->CNT = 10000;
			} else AutoTurnOff(1,0);
		}
	}
}

/*                     MENU POS 1        label average time            */
void BL_MenuPos1(void){
	
	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=14;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BR_MenuPos1(void){
	
	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=8;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_MenuPos1(void){
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=16;
		parameters &= ~(0x1000);//czyszczenie ekranu konca usredniania
			parameters |= 0x0080;
			parameters &= ~(0x0100);
			parameters &= ~(0x0200);
		TIM7->CNT = 10000;
		
	}
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	{
		HAL_DFSDM_FilterRegularStart_DMA(&hdfsdm1_filter0, RecBuff, 2048);
		parameters |= 0x04;
		menuposition=0;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;
	}
}

/*                     MENU POS 2                    */
void BL_MenuPos2(void){
	
	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=7;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BR_MenuPos2(void){
	
	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=9;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_MenuPos2(void){
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	{
	menuposition=17;
	}
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	{
		HAL_DFSDM_FilterRegularStart_DMA(&hdfsdm1_filter0, RecBuff, 2048);
		parameters |= 0x04;
		menuposition=0;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;
	}
}

/*                     MENU POS 3                    */
void BL_MenuPos3(void){
	
	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=8;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BR_MenuPos3(void){
	
	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=10;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_MenuPos3(void){
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=15;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	{
		HAL_DFSDM_FilterRegularStart_DMA(&hdfsdm1_filter0, RecBuff, 2048);
		parameters |= 0x04;
		menuposition=0;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;
	}
}

/*                     MENU POS 4                    */
void BL_MenuPos4(void){
	
	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=9;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BR_MenuPos4(void){
	
	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=11;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_MenuPos4(void){
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=18;
		TIM7->CNT = 10000;	
	}
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	{
		HAL_DFSDM_FilterRegularStart_DMA(&hdfsdm1_filter0, RecBuff, 2048);
		parameters |= 0x04;
		menuposition=0;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;
	}
}

/*                     MENU POS 5                    */
void BL_MenuPos5(void){
	
	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=10;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BR_MenuPos5(void){
	
	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=12;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_MenuPos5(void){
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=19;
		TIM7->CNT = 10000;	
	}
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	{
		HAL_DFSDM_FilterRegularStart_DMA(&hdfsdm1_filter0, RecBuff, 2048);
		parameters |= 0x04;
		menuposition=0;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;
	}
}

/*                     MENU POS 6          System info         */
void BL_MenuPos6(void){
	
	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=11;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BR_MenuPos6(void){
	
	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=13;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_MenuPos6(void){
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=20;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	{
		HAL_DFSDM_FilterRegularStart_DMA(&hdfsdm1_filter0, RecBuff, 2048);
		parameters |= 0x04;
		menuposition=0;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;
	}
}

/*                     MENU POS 7                    */
void BL_MenuPos7(void){
	
	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=12;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BR_MenuPos7(void){
	
	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=14;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_MenuPos7(void){
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=21;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	{
		HAL_DFSDM_FilterRegularStart_DMA(&hdfsdm1_filter0, RecBuff, 2048);
		parameters |= 0x04;
		menuposition=0;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;
	}
}

/*                     MENU POS 8                    */
void BL_MenuPos8(void){
	
	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=13;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BR_MenuPos8(void){
	
	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=7;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_MenuPos8(void){
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	{
		HAL_DFSDM_FilterRegularStart_DMA(&hdfsdm1_filter0, RecBuff, 2048);
		parameters |= 0x04;
		menuposition=0;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	{
		HAL_DFSDM_FilterRegularStart_DMA(&hdfsdm1_filter0, RecBuff, 2048);
		parameters |= 0x04;
		menuposition=0;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;
	}
}
/*                     MENU Backlit                    */
void BL_MenuBacklit(void){
	uint8_t occr;
	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		ResetRtc();
		occr = TIM1->CCR4;
		if (occr<=10) occr=10;
		else TIM1->CCR4 = occr -= 10;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BR_MenuBacklit(void){
	uint8_t occr;
	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		ResetRtc();
		occr = TIM1->CCR4;
		if (occr>=100) occr=100;
		else TIM1->CCR4 = occr += 10;
		TIM7->CNT = 10000;
	
	}
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_MenuBacklit(void){
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	{
	menuposition=9;
	if(CheckFreeSpace(page_lang_backlit,512)<=12) erase_page_bank(ERASE_PAGE_LANG_BACKLIGHT);	
	save_two_int_to_flash(page_lang_backlit,language,TIM1->CCR4);
	TIM7->CNT = 10000;		
	}
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	{
		if(CheckFreeSpace(page_lang_backlit,512)<=12) erase_page_bank(ERASE_PAGE_LANG_BACKLIGHT);	
		save_two_int_to_flash(page_lang_backlit,language,TIM1->CCR4);	
		HAL_DFSDM_FilterRegularStart_DMA(&hdfsdm1_filter0, RecBuff, 2048);
		parameters |= 0x04;
		menuposition=0;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;
	}
}


/*                     MENU SetTimeAverage                    */
void BL_MenuSetTimeAverage(void){
	
	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		if(SET_HOURS_TIME_AVERAGE)
		{
			hour_time_aver--;
			if (hour_time_aver<=-1) 
			{
				hour_time_aver=10;
				min_time_aver=0;
				sec_time_aver=0;
			}
		}
		if(hour_time_aver<10)
		{
			if(SET_MINUTES_TIME_AVERAGE)
			{			
				min_time_aver--;
				if (min_time_aver<=-1) min_time_aver=59;
			}
			if(SET_SECONDS_TIME_AVERAGE)
			{			
				sec_time_aver--;
				if (sec_time_aver<=-1) sec_time_aver=59;
			}	
		}
		TIM7->CNT = 10000;
	}
		
		
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BR_MenuSetTimeAverage(void){
	
	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		if(SET_HOURS_TIME_AVERAGE)
		{
			
			hour_time_aver++;
			if(hour_time_aver==10)
				{
					min_time_aver=0;
					sec_time_aver=0;
				}
			if (hour_time_aver>=11) hour_time_aver=0;
		}
		if(hour_time_aver<10)
		{
			if(SET_MINUTES_TIME_AVERAGE)
			{			
				min_time_aver++;
				if (min_time_aver>=60) min_time_aver=0;
			}
			if(SET_SECONDS_TIME_AVERAGE)
			{			
				sec_time_aver++;
				if (sec_time_aver>=60) sec_time_aver=0;
			}
		}else 
			{
				min_time_aver=0;
				sec_time_aver=0;
			}
		TIM7->CNT = 10000;
	}
		
		
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_MenuSetTimeAverage(void){
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	{
		if(SET_HOURS_TIME_AVERAGE)
		{
			parameters &= ~(0x0080);
			parameters |= 0x0100;
			parameters &= ~(0x0200);
		}else if(SET_MINUTES_TIME_AVERAGE)
		{
			parameters &= ~(0x0080);
			parameters &= ~(0x0100);
			parameters |= 0x0200;
		}else if(SET_SECONDS_TIME_AVERAGE)
		{
			parameters &= ~(0x0080);
			parameters &= ~(0x0100);
			parameters &= ~(0x0200);
			sumsec_time_aver=hour_time_aver*3600+min_time_aver*60+sec_time_aver;
			menuposition = 7;
		}
		
		//left_values = save_data_to_flash(page_end_wsk,76.4,(uint32_t)0x12345678);

		TIM7->CNT = 10000;
	
	}
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	;

}


/*                     MENU display results      17             */
void BL_MenuDisplayResults(void){

	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		parameters ^= 0x0400;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BR_MenuDisplayResults(void){

	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
	parameters ^= 0x0400;
	TIM7->CNT = 10000;	
	}
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_MenuDisplayResults(void){
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=8;
		parameters &= ~(0x0400);
		TIM7->CNT = 10000;
		
	}
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	{
		HAL_DFSDM_FilterRegularStart_DMA(&hdfsdm1_filter0, RecBuff, 2048);
		parameters &= ~(0x0400);
		parameters |= 0x04;
		menuposition=0;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;
	}
}

/*                     MENU set language      18             */
void BL_MenuSetLanguage(void){

	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		if (language == 0) language = 2;
		else language--;
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}
//CopyLastTenResults(page_end_wsk_double,beginlistspace);
void BR_MenuSetLanguage(void){

	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		if (language >=2) language = 0;
		else language++;
		TIM7->CNT = 10000;
	}	
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_MenuSetLanguage(void){
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	{
		if(CheckFreeSpace(page_lang_backlit,512)<=12) erase_page_bank(ERASE_PAGE_LANG_BACKLIGHT);
		save_two_int_to_flash(page_lang_backlit,language,TIM1->CCR4);
		menuposition=10;
		TIM7->CNT = 10000;	
	}
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	{
		if(CheckFreeSpace(page_lang_backlit,512)<=12) erase_page_bank(ERASE_PAGE_LANG_BACKLIGHT);
		save_two_int_to_flash(page_lang_backlit,language,TIM1->CCR4);
		HAL_DFSDM_FilterRegularStart_DMA(&hdfsdm1_filter0, RecBuff, 2048);
		parameters |= 0x04;
		menuposition=0;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;	
	}
}

/*                     MENU user calibration     19             */
void BL_MenuUserCalibration(void){
uint32_t sumtime3;
uint8_t len;
	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		if(CHOOSEN_USER_CALIBRATION)
		{
			parameters &= ~(0x8000);
		
		sumtime3 = hour_time_aver*3600+min_time_aver*60+sec_time_aver;
		if(CheckFreeSpace(page_time_average,512)<=10) erase_page_bank(ERASE_PAGE_US_FA_SUMTIME);
		save_two_int_to_flash(page_time_average,1,sumtime3);
		
		if (CheckFreeSpace(page_user_calib, 512)<512)
		{
		calibration_value2=ReadCalibrationFromFlash(page_user_calib,512);
		} else{
		calibration_value2=ReadCalibrationFromFlash(page_factory_calib,512);
		}
					clear_lcd();
		len=getStringLength_pixels_unicode((void *)language_labels[language][13],&cambria9pt);
		displayText_unicode((128-len)/2,22,(void *)language_labels[language][13],&cambria9pt,1);
		lcd_updatewholedisplay(0);
						HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);	
							HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);		
			menuposition=11;
			TIM7->CNT = 10000;
		}else{
		parameters ^= 0x4000;
		TIM7->CNT = 10000;
		}
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}
//CopyLastTenResults(page_end_wsk_double,beginlistspace);
void BR_MenuUserCalibration(void){

	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		if(CHOOSEN_USER_CALIBRATION)
		{
			parameters &= ~(0x8000);
			HAL_DFSDM_FilterRegularStart_DMA(&hdfsdm1_filter0, RecBuff, 2048);
			menuposition=23;
			TIM7->CNT = 10000;
		}else{
		parameters ^= 0x4000;
		TIM7->CNT = 10000;
		}
	}
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_MenuUserCalibration(void){
	uint32_t sumtime2;
	uint8_t len;
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	{
		if(TYPE_OF_CALIBRATION_DIS)
		{
			parameters |= 0x8000;
			TIM7->CNT = 10000;
		}else{   //WYBRANO KALIBRACJĘ FABYRCZNA
		//read data from flash
		sumtime2 = hour_time_aver*3600+min_time_aver*60+sec_time_aver;
		
		if(CheckFreeSpace(page_time_average,512)<=10) erase_page_bank(ERASE_PAGE_US_FA_SUMTIME);
		save_two_int_to_flash(page_time_average,0,sumtime2);	
		calibration_value2=ReadCalibrationFromFlash(page_factory_calib,512);	//dodanie odczytu z flashu
		clear_lcd();
		len=getStringLength_pixels_unicode((void *)language_labels[language][13],&cambria9pt);
		displayText_unicode((128-len)/2,22,(void *)language_labels[language][13],&cambria9pt,1);
		lcd_updatewholedisplay(0);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);	
							HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);		
		menuposition=11;
		TIM7->CNT = 10000;	
		}
	}
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	{
		HAL_DFSDM_FilterRegularStart_DMA(&hdfsdm1_filter0, RecBuff, 2048);
		parameters |= 0x04;
		menuposition=0;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;
	}
}


/*                     MENU System info     20             */
void BL_MenuSystemInfo(void){

	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=12;
		TIM7->CNT = 10000;	
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}
//CopyLastTenResults(page_end_wsk_double,beginlistspace);
void BR_MenuSystemInfo(void){

	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=12;
		TIM7->CNT = 10000;	
	}
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_MenuSystemInfo(void){
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=12;
		TIM7->CNT = 10000;	
	}
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	{
		HAL_DFSDM_FilterRegularStart_DMA(&hdfsdm1_filter0, RecBuff, 2048);
		parameters |= 0x04;
		menuposition=0;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;
	}
}

/*                     MENU Default settings   21             */
void BL_MenuDefaultSettings(void){

	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=13;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BR_MenuDefaultSettings(void){

	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		erase_page_bank(ERASE_PAGE_LANG_BACKLIGHT);//lang
		erase_page_bank(ERASE_PAGE_DATA_1);//dane1
		erase_page_bank(ERASE_PAGE_DATA_2);//dane2
		erase_page_bank(ERASE_PAGE_US_FA_SUMTIME);//sumtime
		TIM1->CCR4 = 100;
		hour_time_aver=0;
		min_time_aver=30;
		sec_time_aver=0;
		language=0;
		sumsec_time_aver=hour_time_aver*3600+min_time_aver*60+sec_time_aver;
		if (CheckFreeSpace(page_end_wsk, 1024)>=1020)
		{
     for (uint8_t i = 0; i<10; i++)
		 save_data_to_flash(page_end_wsk,0.000,0,1024);
	  }
		
		if(CheckFreeSpace(page_time_average,512)<=10) erase_page_bank(ERASE_PAGE_US_FA_SUMTIME);//sumtime
		save_two_int_to_flash(page_time_average,0,sumsec_time_aver);
		calibration_value2=ReadCalibrationFromFlash(page_factory_calib,512);	//dodanie odczytu z flashu
		parameters &= ~(0x4000);
		clear_lcd();
		displayText_unicode(30,25,"COMPLETED",&cambria9pt,1);
		lcd_updatewholedisplay(0);
				HAL_Delay(200);
			HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
			HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);	
		
		menuposition=13;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;
	
	
	
	}
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_MenuDefaultSettings(void){
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	;
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	;
}

/*                     Warning filters   22             */
void BL_MenuWarningFilters(void){

	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		if(TIM1->CCR4)			//if backlight is on
		{	
			menuposition=0;
			battery_check(menuposition, 1);
			TIM7->CNT = 10000;
		}
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BR_MenuWarningFilters(void){

	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		if(TIM1->CCR4)			//if backlight is on
		{	
			menuposition=0;
			battery_check(menuposition, 1);
			TIM7->CNT = 10000;
		}
	}
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_MenuWarningFilters(void){
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	{
		if(TIM1->CCR4)			//if backlight is on
		{	
			menuposition=0;
			battery_check(menuposition, 1);
			TIM7->CNT = 10000;
		}
	}
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	;
}

/*                     Select Calibration   23             */
void BL_MenuSelectCalibration(void){
uint32_t sumtime4;
uint8_t len;
	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
		{	
		
		if(CheckFreeSpace(page_user_calib,512)<=10) erase_page_bank(ERASE_PAGE_USER_CALIBRATION);//user calibration
		save_data_to_flash(page_user_calib,calibration_value2,16,512);
		
		sumtime4 = hour_time_aver*3600+min_time_aver*60+sec_time_aver;
		if(CheckFreeSpace(page_time_average,512)<=10) erase_page_bank(ERASE_PAGE_US_FA_SUMTIME);//sumtime
		save_two_int_to_flash(page_time_average,1,sumtime4);	
		clear_lcd();
		len=getStringLength_pixels_unicode((void *)language_labels[language][13],&cambria9pt);
		displayText_unicode((128-len)/2,22,(void *)language_labels[language][13],&cambria9pt,1);
		lcd_updatewholedisplay(0);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);		
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);		
		menuposition=11;
		HAL_DFSDM_FilterRegularStop_DMA(&hdfsdm1_filter0);
		TIM7->CNT = 10000;	
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BR_MenuSelectCalibration(void){

	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		uint32_t ind;
		HAL_Delay(200);
		//arm_max_f32(PlayBuff_float, 2048,&calibration_value2, &ind);
		//calibration_value2 = fabsf(calibration_value2);
		//calibration_value2 = Integral_Trap(PlayBuff_float, 48000.f, 2048);
		calibration_value2=rmsa4;
	}
		
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	;
}

void BD_MenuSelectCalibration(void){
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
;
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	;
}


/*                     Backlight_off   24             */
void BL_BacklightOff(void){

	if(BtnFlag_table[BTN_LEFT_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=menuposition_mem;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_LEFT_INDEX] & LONG_PRESS_FLAG)
	{
		menuposition=menuposition_mem;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;
	}
}

void BR_BacklightOff(void){

	if(BtnFlag_table[BTN_RIGHT_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=menuposition_mem;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_RIGHT_INDEX] & LONG_PRESS_FLAG)
	{
		menuposition=menuposition_mem;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;
	}
}

void BD_BacklightOff(void){
	
	if(BtnFlag_table[BTN_DOWN_INDEX] & SHORT_PRESS_FLAG)
	{
		menuposition=menuposition_mem;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;
	}
	if(BtnFlag_table[BTN_DOWN_INDEX] & LONG_PRESS_FLAG)
	{
		menuposition=menuposition_mem;
		battery_check(menuposition, 1);
		TIM7->CNT = 10000;
	}
}



