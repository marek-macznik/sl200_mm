

#include "memory.h"


#include "stm32l4xx_hal_def.h"
#include "stm32l4xx_hal_flash_ex.h"
#include "stm32l4xx_hal_flash.h"
#include "lcdst7565r_unicode.h"
#include "system.h"






const uint32_t *page_end_wsk = 				(void*)0x0801E800;  	/*	Page 59,60  dane_1, dane_2			*/
const uint64_t *page_end_wsk_double = (void*)0x0801E800;		/*	Page 59,60  dane_1, dane_2			*/
const uint64_t *beginlistspace = 			(void*)0x0801D800;		/*	Page 59,60  dane_1, dane_2			*/
const uint32_t *page_lang_backlit =		(void*)0x0801F800;		/*	Page 62			lang,backlight			*/
const uint32_t *page_on_counter =			(void*)0x0801D800;  	/*	Page 58			on, sec_on					*/
const uint8_t  *dateandfirmware = 		(void*)0x08020000; 		/*	Page 63			date, firmware			*/
const uint32_t *page_user_calib = 		(void*)0x0801D000;  	/*	Page 57			user calibration		*/
const uint32_t *page_time_average = 	(void*)0x0801F000;  	/*	Page 61			user/fact,sumtime		*/
const uint32_t *page_factory_calib = 	(void*)0x0801C800;		/*	Page 56			factory calibration	*/
uint16_t errasefactorycalibration=63;




double calibration_value;
double user_calibration_value;
uint64_t language_setting;
uint64_t time_average;
uint64_t average_value;
extern uint16_t parameters;



//uint32_t save

union intoflo
{
	int32_t asssinnt;
	float asssfllo;

}convflo;


FLASH_EraseInitTypeDef Flash_Sett =
{
  TYPEERASE_PAGES,
  0,
  1,                                                            
}; 
//uint8_t Save_Data(uint32_t addr, uint32_t val, uint8_t erase){

// uint32_t stat=1;
// Flash_Sett.PageAddress  = addr;
// HAL_FLASH_Unlock();
// if(erase)
//  HAL_FLASHEx_Erase(&Flash_Sett,&stat); //todo
// HAL_FLASH_Program((uint32_t)0x02,addr, val);
// HAL_FLASH_Lock();
// return stat;
//}

void erase_page(uint16_t page)   //156 ex.c
{
__disable_irq();
uint32_t page_error = 0xFFFFFFFF;

FLASH_EraseInitTypeDef Flash_EraseInit;

Flash_EraseInit.TypeErase = FLASH_TYPEERASE_PAGES;
Flash_EraseInit.Banks =FLASH_BANK_2;
Flash_EraseInit.Page=page;
Flash_EraseInit.NbPages=1;
	

HAL_FLASH_Unlock();		
HAL_FLASHEx_Erase(&Flash_EraseInit,&page_error);


	
CLEAR_BIT(FLASH->ACR, FLASH_ACR_ICEN);
CLEAR_BIT(FLASH->ACR, FLASH_ACR_ICRST);
SET_BIT(FLASH->ACR, FLASH_ACR_ICEN);

CLEAR_BIT(FLASH->ACR, FLASH_ACR_DCEN);
CLEAR_BIT(FLASH->ACR, FLASH_ACR_DCRST);
SET_BIT(FLASH->ACR, FLASH_ACR_DCEN);
	
HAL_FLASH_Lock();
__enable_irq();		
}

void erase_page_bank(uint16_t page)   //156 ex.c
{
__disable_irq();
uint32_t page_error = 0xFFFFFFFF;

FLASH_EraseInitTypeDef Flash_EraseInit;

Flash_EraseInit.TypeErase = FLASH_TYPEERASE_PAGES;
Flash_EraseInit.Banks =FLASH_BANK_1;
Flash_EraseInit.Page=page;
Flash_EraseInit.NbPages=1;
	

HAL_FLASH_Unlock();		
HAL_FLASHEx_Erase(&Flash_EraseInit,&page_error);


	
CLEAR_BIT(FLASH->ACR, FLASH_ACR_ICEN);
CLEAR_BIT(FLASH->ACR, FLASH_ACR_ICRST);
SET_BIT(FLASH->ACR, FLASH_ACR_ICEN);

CLEAR_BIT(FLASH->ACR, FLASH_ACR_DCEN);
CLEAR_BIT(FLASH->ACR, FLASH_ACR_DCRST);
SET_BIT(FLASH->ACR, FLASH_ACR_DCEN);
	
HAL_FLASH_Lock();
__enable_irq();		
}




uint16_t save_data_to_flash(const uint32_t *adr_pg, float a, uint32_t b, uint16_t sizee)
{
	
	uint64_t save_data;
	uint32_t wart;
	uint16_t left=0;
	

	

		union calosc {
		uint64_t duzawarosc;
		struct skladowe_dane nazwa;
	};
	

	union calosc addr2;
	addr2.nazwa.part1=a;
	addr2.nazwa.part2=b;

	save_data= addr2.duzawarosc;
	
	wart=0xFFFFFFFF;
	while(wart == 0xFFFFFFFF && left<=sizee)
	{
		adr_pg--;
		wart = *adr_pg;
		left++;
	}
	
	adr_pg++;
	
__disable_irq();	
HAL_FLASH_Unlock();		
HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD,(uint32_t)adr_pg, save_data);
HAL_FLASH_Lock();
__enable_irq();	
	
	return (left-3);
	
}

uint16_t save_two_int_to_flash(const uint32_t *adr_pg, uint32_t a, uint32_t b)
{
	
	uint64_t save_data;
	uint32_t wart;
	uint16_t left=0;
	

	

		union calosc {
		uint64_t duzawarosc;
		struct link_two_variables nazwa;
	};
	

	union calosc addr2;
	addr2.nazwa.part1=a;
	addr2.nazwa.part2=b;

	save_data= addr2.duzawarosc;
	
	wart=0xFFFFFFFF;
	while(wart == 0xFFFFFFFF && left<=512)
	{
		adr_pg--;
		wart = *adr_pg;
		left++;
	}
	
	adr_pg++;
	
__disable_irq();	
HAL_FLASH_Unlock();		
HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD,(uint32_t)adr_pg, save_data);
HAL_FLASH_Lock();
__enable_irq();	
	
	return (left-3);
	
}


uint16_t CheckFreeSpace(const uint32_t *adr_pg, uint16_t space)
{
	uint32_t comp_wart;
	uint16_t left=0;
	
	comp_wart=0xFFFFFFFF;
	
	while(comp_wart == 0xFFFFFFFF && left<=space)
	{
		adr_pg--;
		comp_wart = *adr_pg;
		left++;
	}

	return (left-1);
}


void DisplayListDb(const uint32_t *adr_pg, uint16_t space)
{
	uint32_t comp_wart;
	uint16_t left=0;
	uint32_t time_in_sec;
	uint32_t value_db_disp;
	float			value_db_disp_fl;
	char buffer10[40];
	uint8_t h;
	uint8_t m;
	uint8_t s;

	
	union intoflo
	{
	int32_t asssinnt;
	float asssfllo;

	}convflo;
	
	comp_wart=0xFFFFFFFF;
	
	
	while(comp_wart == 0xFFFFFFFF && left<=space)
	{
		adr_pg--;
		comp_wart = *adr_pg;
		left++;
	}
	
	if (DISPLAY_LIST_RESULTS_1_2)
	{
		adr_pg -= 10;
		//displayText_unicode(40,50,"RESULTS",&cambria_9ptA,1);
		for(int8_t i = 4, k=6 ; i >= 0; i--,k++)
			{
				time_in_sec=*adr_pg;
				h = time_in_sec / 3600;
				m = (time_in_sec - 3600 * h) / 60;
				s = time_in_sec - 3600 * h - 60 * m;
		
		
				sprintf(buffer10,"%01d. %02d : %02d : %02d",k, h, m, s);
				displayText_unicode(5,i*10,buffer10,&cambria_9ptA,1);
				adr_pg--;
				value_db_disp=*adr_pg;
				convflo.asssinnt=value_db_disp;
				value_db_disp_fl = convflo.asssfllo;
				adr_pg--;
				sprintf(buffer10,"%3.1fdB",value_db_disp_fl);
				displayText_unicode(80,i*10,buffer10,&cambria_9ptA,1);
			}
	
	}else
		{
		//	displayText_unicode(40,50,"RESULTS",&cambria_9ptA,1);
			for(int8_t i = 4, k=1 ; i >= 0; i--,k++)
			{
				time_in_sec=*adr_pg;
				h = time_in_sec / 3600;
				m = (time_in_sec - 3600 * h) / 60;
				s = time_in_sec - 3600 * h - 60 * m;
		
		
				sprintf(buffer10,"%01d. %02d : %02d : %02d",k, h, m, s);
				displayText_unicode(5,i*10,buffer10,&cambria_9ptA,1);
				adr_pg--;
				value_db_disp=*adr_pg;
				convflo.asssinnt=value_db_disp;
				value_db_disp_fl = convflo.asssfllo;
				adr_pg--;
				sprintf(buffer10,"%3.1fdB",value_db_disp_fl);
				displayText_unicode(80,i*10,buffer10,&cambria_9ptA,1);
			}
		}
}

/* 				kopiowanie danych z jednego miejsca pamięci do drugiego
		*adr_begin_page	-	miejsce do którego mają być skopiowane dane
		*adr_pg					-	miejsce startowe od którego mam być sprawdzona pamięć   */
										  
void CopyLastTenResults(const uint64_t *adr_pg, const uint64_t *adr_beging_page)
{
	uint64_t save_data;
	uint32_t wart;
	uint16_t left=0;
	
	__disable_irq();	
HAL_FLASH_Unlock();	
	
	wart=0xFFFFFFFF;
	while(wart == 0xFFFFFFFF && left<=512)
	{
		adr_pg--;
		wart = *adr_pg;
		left++;
	}
	
	adr_pg -=9;
	
	save_data = *adr_pg ;
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD,(uint64_t)adr_beging_page, save_data);
	
	
	for (uint8_t i = 0; i<9 ; i++)
	{
		adr_pg++;
		save_data = *adr_pg ;
		adr_beging_page++;
		
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD,(uint32_t)adr_beging_page, save_data);
	
	
	}
	
HAL_FLASH_Lock();
__enable_irq();	
}


uint32_t ReadFirstOrSecondValFromFlash(const uint32_t *adr_pg, uint16_t space, uint8_t first_second)
{
	uint32_t comp_wart;
	uint16_t left=0;
	uint32_t value;
	
	comp_wart=0xFFFFFFFF;
	
	while(comp_wart == 0xFFFFFFFF && left<=space)
	{
		adr_pg--;
		comp_wart = *adr_pg;
		left++;
	}
	
	if(first_second==0) value=*adr_pg;
	else value=*--adr_pg;
		
	
	return (value);
}

float ReadCalibrationFromFlash(const uint32_t *adr_pg, uint16_t space)
{
	uint32_t comp_wart;
	uint16_t left=0;
	float value2;
	uint32_t value_db_disp2;
	
	union intoflo
	{
	int32_t asssinnt;
	float asssfllo;

	}convflo;
	
	
	comp_wart=0xFFFFFFFF;
	
	while(comp_wart == 0xFFFFFFFF && left<=space)
	{
		adr_pg--;
		comp_wart = *adr_pg;
		left++;
	}
	
	adr_pg--;
	value_db_disp2=*adr_pg;
	convflo.asssinnt=value_db_disp2;	
	value2=convflo.asssfllo;
	
	return value2;
}


float DisplayLastDB(const uint32_t *adr_pg, uint16_t space)
{
	uint32_t comp_wart;
	uint16_t left=0;
	uint32_t value_db_disp;
	float			value_db_disp_fl;

	
	union intoflo
	{
	int32_t asssinnt;
	float asssfllo;

	}convflo;
	
	comp_wart=0xFFFFFFFF;
	
	
	while(comp_wart == 0xFFFFFFFF && left<=space)
	{
		adr_pg--;
		comp_wart = *adr_pg;
		left++;
	}
	
	adr_pg--;
	value_db_disp=*adr_pg;
	convflo.asssinnt=value_db_disp;
	value_db_disp_fl = convflo.asssfllo;
	
	return value_db_disp_fl;
}

void DisplayVerDate(const uint8_t *adr_pg, uint8_t xdate, uint8_t ydate, uint8_t xfirmware, uint8_t yfirmware)
{
	uint8_t date;
	char buffer10[5];
	

	
	adr_pg -=7;
	for(int8_t i = 1 ; i < 7; i++)
	{
		adr_pg++;
		date=*adr_pg;
		
		if(i==1)
		{
			sprintf(buffer10,"%02x.",date);
			displayText_unicode(xfirmware,yfirmware,buffer10,&cambria_9ptA,1);
		}
		if(i==2)
		{
			sprintf(buffer10,"%02x",date);
			displayText_unicode(xfirmware+15,yfirmware,buffer10,&cambria_9ptA,1);
		}
		if(i==3)
		{
			sprintf(buffer10,"%02x.",date);
			displayText_unicode(xdate,ydate,buffer10,&cambria_9ptA,1);
		}
		if(i==4)
		{
			sprintf(buffer10,"%02x.",date);
			displayText_unicode(xdate+15,ydate,buffer10,&cambria_9ptA,1);
		}
		if(i==5)
		{
			sprintf(buffer10,"%02x",date);
			displayText_unicode(xdate+30,ydate,buffer10,&cambria_9ptA,1);
		}
		if(i==6)
		{
			sprintf(buffer10,"%02x",date);
			displayText_unicode(xdate+42,ydate,buffer10,&cambria_9ptA,1);
		}


	}
	


}




