/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2016 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "adc.h"
#include "dfsdm.h"
#include "dma.h"
#include "iwdg.h"
#include "sai.h"
#include "tim.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "lcdst7565r_unicode.h"
#include "buttons.h"
#include "memory.h"
#include "system.h"
#include "filters.h"
#include "display.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
uint16_t czas;
uint16_t time_aver;
uint32_t batbuff[20];
uint32_t voltage;
uint32_t time_backlight;
uint8_t back_one;
uint8_t pomoc;
uint8_t firsttime;




/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_DFSDM1_Init();
  MX_SAI1_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  MX_TIM16_Init();
  MX_TIM17_Init();
  MX_IWDG_Init();

  /* USER CODE BEGIN 2 */	
	DBGMCU->APB1FZR1 |= DBGMCU_APB1FZR1_DBG_IWDG_STOP;
	/*****************************************************************/
	/*	Odczyt danych z pamięci Flash. Backlight and Language	POCZĄTEK */
	/*****************************************************************/
		if (CheckFreeSpace(page_lang_backlit, 512)<512)	// check if memory is not empty
	{
		TIM1->CCR4 = ReadFirstOrSecondValFromFlash(page_lang_backlit,512,0);  //odczytaj wartość pwm dla podswietlenia wyswietlacza
		language = ReadFirstOrSecondValFromFlash(page_lang_backlit,512,1);    // odczytaj jezyk 
	}else
	{
		TIM1->CCR4 = 100;		//	IF EMPTY, read default value 	100% backlight        
		language = 0;				//	IF EMPTY, read default value 	english language
	}
	/*****************************************************************/
	/*	Odczyt danych z pamięci Flash. Backlight and Language	KONIEC */
	/*****************************************************************/



	/*****************************************************************/
	/*	Obsługa zewnętrznego kwarcu zegarkowego 32768Hz	POCZĄTEK */
	/*****************************************************************/

	if (__HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST) != RESET)                 /* jesli proba odpalenia kwarcu zewnętrzego była nie udana wystartuj wewnętrzny układ RC */
  {
		PWR->CR1 |= (1<<8);
	
		RCC->BDCR |= (1<<16);  //Resets the entire Backup domain
		HAL_Delay(20);
		RCC->BDCR &= ~(1<<16); //Resets not activated
		HAL_Delay(20);
	

		GPIOA->BSRR |= GPIO_PIN_3;
		RCC->BDCR |= (1<<9);   //LSI OSCILATOR CLOCK USED AS RTC
		RCC->BDCR |= (1<<15);  //RTC CLOCK ENABLE
		PWR->CR1 &= ~(1<<8);
	}else
		{
			PWR->CR1 |= (1<<8);
	
			RCC->BDCR |= (1<<16);  //Resets the entire Backup domain
			HAL_Delay(20);
			RCC->BDCR &= ~(1<<16); //Resets not activated
			HAL_Delay(20);
	
			RCC->BDCR |= (1<<4);   //HIGH DRIVING XTAL CAPABILITY
			RCC->BDCR |= (1<<0);   //LSE OSCILATOR ON
			GPIOA->BSRR |= GPIO_PIN_3;
			while ((RCC->BDCR & RCC_BDCR_LSERDY) == 0)
			{
	       /* jeśli proba odpalenia nie powiedzie się zadziała reset od watchdoga i uruchomiony zostanie LSI */
			}
			GPIOA->BRR |= GPIO_PIN_3;
			RCC->BDCR |= (1<<8);   //LSE OSCILATOR CLOCK USED AS RTC
			RCC->BDCR |= (1<<15);  //RTC CLOCK ENABLE
			RCC->BDCR |= (1<<25);   //LSE CLOCK OUTPUT PIN
			PWR->CR1 &= ~(1<<8);
			HAL_Delay(250);
			HAL_IWDG_Refresh(&hiwdg);
			HAL_Delay(200);
		}	
	/*****************************************************************/
	/*	Obsługa zewnętrznego kwarcu zegarkowego 32768Hz	KONIEC */
	/*****************************************************************/

	HAL_IWDG_Refresh(&hiwdg);
	lcd_init();			// inicjalizacja wyswietlacza
	
	HAL_TIM_Base_Start(&htim1);				// pwm dla podswietlenia
	HAL_TIM_PWM_Start_IT(&htim1,TIM_CHANNEL_4);
	
	HAL_TIM_Base_Start(&htim2);				// timer odliczajacy czas do automatycznego wylaczenia podswietlenia wyswietlacza
	HAL_TIM_Base_Start(&htim7);				// refresh rate for display results
	HAL_TIM_Base_Start(&htim6);       // for debuging,  not used in meter measurement functions
	HAL_TIM_Base_Start(&htim16);
	HAL_TIM_Base_Start(&htim17);
	
	HAL_TIM_OC_Start_IT(&htim16, TIM_CHANNEL_1);
	HAL_TIM_OC_Start_IT(&htim17, TIM_CHANNEL_1);
		
	displayText_unicode(0,0,Caisson_logo,&icon,1); // wyswietlanie ekranu
	lcd_updatewholedisplay(0);

	min_time_aver=30;
	calibration_value2=5.3880e-004;
	
	
	HAL_IWDG_Refresh(&hiwdg);
	
	//while(1) { HAL_IWDG_Refresh(&hiwdg); } ; // USUN
	
	if (CheckFreeSpace(page_time_average, 512)<512)
	{
		ConvertSecondsToHMS(ReadFirstOrSecondValFromFlash(page_time_average,512,0));  //odczytaj sekundy, minuty i godziny czasu kalibracji
		if(ReadFirstOrSecondValFromFlash(page_time_average,512,1)==1)parameters |= 0x4000;    // set TYPE_OF_CALIBRATION_DIS flag. It means that meter reads user calibration, not factory
	}
	
	
	
	
	if (CheckFreeSpace(page_factory_calib, 512)<512)				// if page FACTORY CLIBRATION is  not empty
	{
			menuposition=0;    // go to menu "display result"
	}else 
		{
			if (CheckFreeSpace(page_factory_calib, 512)<512)      // dla pewnosci sprawdz jeszcze raz
			{
				menuposition=0;   // go to menu "display result"
			}else    // if page Factory calibration is empty
				{
					erase_page_bank(ERASE_PAGE_FACTORY_CALIBRATION);     // just for secure  -> errase this page
					menuposition=6;     // go to menu calibration
				}
		}
	


	
	
	if(TYPE_OF_CALIBRATION_DIS)     //wybór odczytu kalibracji    1-user calibration    2-factory calibration
	{
		if (CheckFreeSpace(page_user_calib, 512)<512)  //odczytanie kalibracji użytkownika
		{
		
		calibration_value2=ReadCalibrationFromFlash(page_user_calib,512);
		} else{
		calibration_value2=5.3430e-004;
		}
	}else
	{
		if (CheckFreeSpace(page_factory_calib, 512)<512)  //odczytanie kalibracji fabrycznej
		{
			calibration_value2=ReadCalibrationFromFlash(page_factory_calib,512);
		} else 
		{
			calibration_value2=5.3430e-004;   // nigdy nie powinna wykonać się ta linia.  Jeśli się wykona, oznacza to, że w jakiś sposób skasowania została kalibracjja fabryczna.
		}
	}

	
	
	
	if (CheckFreeSpace(page_on_counter, 512)<512)   // sprawdz czy są dane na tej stronie
	{
		total_turn_on=ReadFirstOrSecondValFromFlash(page_on_counter,512,1);    // odczyt ilości włączeń miernika
		time_one_on=ReadFirstOrSecondValFromFlash(page_on_counter,512,0);      // odczyt całkowitego czasu działania miernika w sekundach
	}else
		{
			total_turn_on=0;    // jesli nie ma danych, przyjmij domyślne wartości
			time_one_on=0;
		}
		total_turn_on++;
		
		if(total_turn_on==1)  // wykonaj tylko podczas pierwszego włączenia miernika
		{
		erase_page_bank(ERASE_PAGE_USER_CALIBRATION);   // czysc strone kalibracji użytkownika
		erase_page_bank(ERASE_PAGE_ON_SECON);						// czysc strone ilosci wlaczen i calkowitego czasu pracy mirnika
		erase_page_bank(ERASE_PAGE_DATA_1);							// pierwsza stona pamieci wynikow
		erase_page_bank(ERASE_PAGE_DATA_2);									
		erase_page_bank(ERASE_PAGE_US_FA_SUMTIME);	    // czysc 
		erase_page_bank(ERASE_PAGE_LANG_BACKLIGHT);
		total_turn_on++;  //  
		}


	
	if (CheckFreeSpace(page_end_wsk, 1024)>=1020)				// wykonywanie podczas pierwszego uruchomienia miernika 
	{
	for (uint8_t i = 0; i<10; i++)
		save_data_to_flash(page_end_wsk,0.000,0,1024);    // wypełnij zerami pierwsze 10 pozycji w menu gdzie odczytuje się wyniki pomiarów.
	}
	
	sumsec_time_aver=hour_time_aver*3600+min_time_aver*60+sec_time_aver;
	

HAL_IWDG_Refresh(&hiwdg);
	FiltersInit();
HAL_IWDG_Refresh(&hiwdg);
	



	HAL_ADC_Start_DMA(&hadc1,batbuff,20);
	HAL_ADC_Start(&hadc1);

	HAL_Delay(200);
	HAL_IWDG_Refresh(&hiwdg);
	HAL_Delay(200);
	HAL_IWDG_Refresh(&hiwdg);
	HAL_Delay(200);
	HAL_IWDG_Refresh(&hiwdg);
	HAL_Delay(200);
	HAL_IWDG_Refresh(&hiwdg);

	clear_lcd();


	

	
	parameters |= 0x04;    	//CLEAR_MIN_MAX_1000MS
	parameters |= 0x08;			//CLEAR_MIN_MAX_AVERAGE
	parameters &= ~(0x40);	//clear_dfsdm
	
	
	TIM2->CNT = 0;
	
	
  if(TIM1->CCR4 < 10)
	{
		TIM1->CCR4 = 100;
	}
 


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

		HAL_IWDG_Refresh(&hiwdg);
		
		
		if(firsttime != 0) 
		{
			battery_check(menuposition, 0);
			DisplayBar(menuposition);
		}
		
		SetButtonsFunc(menuposition);	
		ButtonFunc_Exe();
		

		
		if(menuposition==1)   // aktywne menu usredniania wyniku
		{
			RTC_TimeShow();
		}
		
		if (TIME_WEIGHTING_CURVE) time_aver=10000;   // 1s
		else time_aver=5000;   //0.5s
		
	
		czas = TIM7->CNT;
		if (czas>=time_aver)  //refresh rate   1s or 0.5s
		{
			czas=0;
			TIM7->CNT = 0;
			
			DisplayFinishedTimeAverage();
			display_results();
			
			AutoTurnOff(0,0);    
			AutoBacklightOff();

			firsttime=1;
		}
  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 40;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_SAI1|RCC_PERIPHCLK_DFSDM1
                              |RCC_PERIPHCLK_ADC;
  PeriphClkInit.Sai1ClockSelection = RCC_SAI1CLKSOURCE_PLLSAI1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
  PeriphClkInit.Dfsdm1ClockSelection = RCC_DFSDM1CLKSOURCE_PCLK;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_MSI;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 1;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 52;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV17;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV8;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_SAI1CLK|RCC_PLLSAI1_ADC1CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }

  HAL_RCCEx_EnableLSCO(RCC_LSCOSOURCE_LSI);

  __HAL_RCC_PWR_CLK_ENABLE();

  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }

  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	uint8_t i;
	static uint32_t batval;
	for(i=0; i<20; i++)
	{
		batval += batbuff[i];
	}
	voltage = batval/20;
	batval=0;
	
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
