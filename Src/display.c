#include "stm32l4xx_hal.h"
#include "lcdst7565r_unicode.h"
#include "system.h"
#include "filters.h"
#include "buttons.h"
//#include "rtc.h"
#include "memory.h"

 float min_val_aver;
 float max_val_aver;
 float min_val;   
 float max_val;


static float min_val_aver_mem;
static float max_val_aver_mem;
volatile int8_t hour_time_aver;
volatile int8_t min_time_aver;
volatile int8_t sec_time_aver;
volatile uint32_t sumsec_time_aver;
uint32_t time_sec_main;

const char* language_labels[3][29]={
	{	"Measurement time",		//0	1
		"View results", 			//1 2
		"Backlight",					//2	3			
		"Language", 					//3 4
		"Calibration",				//4 5
		"System information", //5	6	
		"Default settings",		//6 7
		"Exit",								//7 8
		"MENU",								//8	9
		"MEASUREMENT TIME",		//9			
		"RESULTS",						//10		
		"NEW",							//11
		"CURRENT",					//12
		"SAVED",						//13
		"CALIBRATION TYPE", //14   
		"Factory", 			//15
		"User", 				//16
		"YES", 					//17
		"NO",						//18 okokok
		"English",			//19
		"STOP?",				//20
		"SAVE",					//21
		"START",				//22
		"SET CALIBRATION",//23
		"version:",				//24
		"date:",					//25
		"counter:",				//26
		"sec:",						//27
		"SWITCH OFF?"	},  //28
	
	{	"Zeitmittelung",  		//0 1  29
		"Werte anzeigen", 		//1 2  30
		"Anzeigehelligkeit",	//2 3
		"Sprache", 						//3 4
		"Kalibrierung",     	//4 5
		"System information",	//5	6	 	
		"Zurucksetzen", 			//6 7  Zurücksetzen
		"Ausgang",		//7
		"MENU",				//8 menü
		"ZEIT",	      //9
		"ERGEBNISSE",	//10	
		"NEU",				//11
		"AKTUELL",	    //12 strom 
		"GESPEICHERT",    //13SICHERN
		"BAUART",     //14
		"Werkseinstellung",//15
		"User",       //16
		"JA",         //17
		"NICHT",      //18
		"Deutsch",		//19
		"STOPPEN?",		//20
		"SPAREN",			//21
		"START",    	//22start
		"SET KALIBRIERUNG",//23
		"Version:",	//24
		"Datum:",		//25
		"Zahler:",		//26
		"sec:",			//27
		"AUSSCHALTEN?"//28
	},
	
	{	"Tijdmiddeling", 
		"Zie resultaten",
		"Backlight",				
		"Taal", 		
		"Kalibratie", 		
		"Systeeminformatie",
		"Resetten",	
		"Exit",	
		"MENU",		
		"TIJD",	
		"RESULTATEN",		
		"NIEUW",
		"AKTUEEL",			
		"OPGESLAGEN",			
		"TYPE SELECTEREN",			
		"Fabrieksinstelling",			
		"Gebruiker",
		"JA",
		"NEE",
		"Nederlands",
		"STOP?",
		"OPSLAAN",
		"START",
		"SET KALIBRATIE",
		"versie:",	
		"datum:",
		"teller:",
		"sec:",
		"UITZETTEN?"}
};







const char* warnings[3][5]={
{	{"Option Locked"}, 
	{"when measurement"},
	{"is active!"},
	{""},
	{""}},

{	{"Option gesperrt"},
	{"wenn im"}, 
	{"durchschnittlich"}, 
	{"its aktiv!"}, 
	{""}},

{	{"Optie vergrendeld"}, 				
	{"wanneer middeling"}, 
	{"is actief!"}, 								
	{""}, 
	{""}}

};

const char* warnings2[3][3]={
{	{"Averaging is"}, 
	{"running!"},
	{""}},

{	{"Durchschnittlich"},
	{"its aktiv!"}, 
	{""}},
   
{	{"Middeling is"}, 				
	{"actief!"}, 
	{""}}

};

const char* warnings3[3][5]={
{	{"Reset to default"}, 
	{"setting?"},
	{""},
	{""},
	{""}},

{	{"Auf"},
	{"Werkseinstellung"}, 
	{"zurucksetzen?"}, //zurücksetzen?
	{""}, 
	{""}},
   
{	{"Resetten"}, 				
	{"naar"}, 
	{"fabrieksinstelling?"}, 								
	{""}, 
	{""}}

};



void RTC_TimeShow(void);
void RTC_Reset_time(void);
void RTC_RealTimeShow(void);
void DisplayFinishedTimeAverage(void);
void display_results(void);
void DisplayBar(uint8_t menu);
void DisplayShortMessage(void);


 void RTC_TimeShow(void)
{
	char buffer_time[20];
	
	uint32_t time_to_display;
	uint8_t h;
	uint8_t m;
	uint8_t s;
	
	if(AVERAGE_TIME_REACHED) 
	{
		sprintf(buffer_time,"%02d:%02d:%02d",hour_time_aver, min_time_aver, sec_time_aver);
		rectangle (92,52,8,2,0);
		rectangle (90,52,8,2,1);
		rectangle (94,52,8,2,1);	
	}
	else if (!(DISPLAY_TIME_AVERAGE))
		{
			sprintf(buffer_time,"%02d:%02d:%02d",0, 0, 0);
			rectangle (90,52,8,2,1);
			rectangle (94,52,8,2,1);
		}
		else 
			{
				time_to_display = GetSecondsRtc();
				h = time_to_display/ 3600;
				m = (time_to_display - 3600 * h) / 60;
				s = time_to_display - 3600 * h - 60 * m;
			
				sprintf(buffer_time,"%02d:%02d:%02d",h, m, s);
				rectangle (90,52,8,8,0);
				rectangle (90,52,8,1,1);
				rectangle (91,53,6,1,1);
				rectangle (92,54,4,1,1);
				rectangle (93,55,2,1,1);
			}

	rectangle (35,50,11,54,0);
	displayText_unicode(38,49,buffer_time,&cambria9pt,1);
	
} 

 void RTC_Reset_time(void)
{
//	RTC_TimeTypeDef sTime;
  
//  sTime.Hours = 0x0;
 // sTime.Minutes = 0x0;
 // sTime.Seconds = 0x0;
  
//  HAL_RTC_SetTime(&hrtc, &sTime, FORMAT_BCD);
}

 void RTC_RealTimeShow(void)
{
	char buffer_time[20];
	uint32_t total_seconds;
	uint8_t hrs1;
	uint8_t min1;
	uint8_t sec1;
	
	
	total_seconds = GetSecondsRtc();
	
	hrs1 = total_seconds / 3600;
	min1 = (total_seconds - 3600 * hrs1) / 60;
	sec1 = total_seconds - 3600 * hrs1 - 60 * min1;
	
	
	
		
		sprintf(buffer_time,"%02d:%02d:%02d",hrs1, min1, sec1);
		rectangle (35,2,11,58,0);
		displayText_unicode(40,1,buffer_time,&cambria9pt,1);
	
} 


void DisplayFinishedTimeAverage(void)
{
	

	if(AVERAGING_IS_ACTIVE)
	{
		
		time_to_off=5;   // always 5 to prevent turn off meter
		sumsec_time_aver=hour_time_aver*3600+min_time_aver*60+sec_time_aver;

		if(GetSecondsRtc() >= sumsec_time_aver)    // compare actual value from RTC to value "measurement time" choosen from menu
		{
			parameters |= 0x1000;  //AVERAGE_TIME_REACHED flag set
			if(AVERAGE_TIME_REACHED)
			{
				if(CheckFreeSpace(page_end_wsk,1024) <= 20)   // if memory space in memory page for result is become full 
				{
					erase_page_bank(ERASE_PAGE_DATA_1);     
					CopyLastTenResults(page_end_wsk_double,beginlistspace);    //copy last ten results from second measurement page to first measurement page
					erase_page_bank(ERASE_PAGE_DATA_2);
				}
				
				save_data_to_flash(page_end_wsk,(float)artm_db_result_inf,sumsec_time_aver,1024);
			}
			min_val_aver_mem=min_val_aver;
			max_val_aver_mem=max_val_aver;
			parameters &= ~(0x0800);    // DISPLAY_TIME_AVERAGE  flag reset  
			parameters &= ~(0x2000);    // AVERAGING_IS_ACTIVE   flag reset
			ResetRtc();
		}
	}
}

void DisplayBar(uint8_t menu)
{
	float bar_result;	
	if(menu==0 )    // menuposition 0
	{
		if(TIME_WEIGHTING_CURVE) bar_result=(artm_db_result_1000ms_2/2.5f)-11;
		else bar_result=(artm_db_result_125ms/2.5f)-11;
		for(uint8_t k=1; k<=40;k++)
		{
			if(bar_result>k) rectangle(k*3,4,4,2,1);
			else rectangle(k*3,4,4,2,0);
		}

		
	}
	
	
	if(menu==1)  // menuposition 1
	{
		
	if(WAIT_UNTIL_CLR_AVERAGE || !(DISPLAY_TIME_AVERAGE))
	{
	}else{
			bar_result=(artm_db_result_125ms/2.5f)-11;
		for(uint8_t k=1; k<=40;k++)
		{
			if(bar_result>k) rectangle(k*3,4,4,2,1);
			else rectangle(k*3,4,4,2,0);
		}
	}
}
	
	


		lcd_updatewholedisplay(0);


}




 void RTC_Reset_time_buttons(void)
{
	if (!(DISPLAY_TIME_AVERAGE))
	{

	}
}

void display_results(void)
{
static	char buffer4[40];	
static	char dig[4];
static	char dig2[4];
static	char frac[2];	  
static	char frac2[2];
static char min_max_buff[20];
	char bkl[10];
	char bkl2[5];
	char bkl3[5];
	uint8_t backlite_1;
static	uint16_t i;
	uint8_t len;
	static double range_db_disp;
	
	
	rectangle (1,0,52,126,0);
	if (menuposition != 1)rectangle (29,52,11,98,0);
	

	for(i=0;i<40;i++)
	{
	 buffer4[i]=0;
	}
	
	if (menuposition==0)
	{/*   Wyswietlanie menu głównego		*/

			sprintf(buffer4,"%3.4f",artm_db_result_1000ms);
			range_db_disp=artm_db_result_1000ms;


			/* Rysowanie linjiki (pionowe linie) */
				rectangle (3,0,3,1,1);
				for(uint8_t n=15; n<=130;n+=12) // n_max 
				{
					rectangle (n,0,3,1,1);
				}
				
	
		
	} 
	else if (menuposition==1)
	{/*   Wyswietlanie menu uśredniania		*/
		sprintf(buffer4,"%3.4f",artm_db_result_inf);
		range_db_disp=artm_db_result_inf;
		rectangle (3,0,3,1,1);
		for(uint8_t n=15; n<=130;n+=12)
		{
			rectangle (n,0,3,1,1);
		}
		
	}	
	else if (menuposition==6)
	{/*	 Wyswietlanie menu kalibracji    */
	}
	
	if (menuposition==0 || menuposition==1 )
	{	
		if(range_db_disp<100.000f)   /*	 Jesli wynik do wyswietlenia jest    */
		{
			
			for (i=0;i<2;i++)
			{
				dig[i]=buffer4[i];
			}
			
			frac[0] = buffer4[3];	  
			displayText_unicode(51,17,".",&calibri26pt,1);
			displayText_unicode(15,17,dig,&calibri26pt,1);
			displayText_unicode(60,17,frac,&calibri26pt,1);
			displayText_unicode(80,17,"dB",&calibri26pt,1);
		}	
		else
		{
			
			for (i=0;i<3;i++)
			{
				dig2[i]=buffer4[i];
			}
			
			frac2[0] = buffer4[4];	  
			displayText_unicode(61,17,".",&calibri26pt,1);
			displayText_unicode(8,17,dig2,&calibri26pt,1);
			displayText_unicode(70,17,frac2,&calibri26pt,1);
			displayText_unicode(90,17,"dB",&calibri26pt,1);
	  }
 

		if (menuposition==0)
		{
			if(CLEAR_MIN_MAX_1000MS )
			{	
				min_val=150;
				max_val=20;
				displayText_unicode(7,7,"min:   --.-",&cambria_9ptA,1);
				displayText_unicode(70,7,"max:   --.-",&cambria_9ptA,1);
				parameters ^= 0x04;	
			}
			else
			{
					sprintf(min_max_buff,"min: %3.1f",min_val);
					displayText_unicode(7,7,min_max_buff,&cambria_9ptA,1);
					sprintf(min_max_buff,"max: %3.1f",max_val);
					displayText_unicode(70,7,min_max_buff,&cambria_9ptA,1);
			}					
			if(WAIT_UNTIL_CLR_AVERAGE)
			{
				min_val_aver=150;
				max_val_aver=20;
			}
			if(TIME_WEIGHTING_CURVE) 
			{
				DisplayBar(menuposition);
			}
			if (AUDIO_WEIGHTING_CURVE)
			{
				displayText_unicode(113,50,"C",&cambria9pt,1);
			}
			else
			{
				displayText_unicode(112,50,"A",&cambria9pt,1);
			}
			if (TIME_WEIGHTING_CURVE)
			{
				displayText_unicode(55,50,"SLOW",&cambria9pt,1);
			}
			else
			{
				displayText_unicode(55,50,"FAST",&cambria9pt,1);
			}
		} 
		if (menuposition==1)
		{	
			if(WAIT_UNTIL_CLR_AVERAGE || !(DISPLAY_TIME_AVERAGE))
			{
				rectangle (1,8,40,126,0);
				//rectangle(1,2,12,125,0);
				rectangle(1,4,4,126,0);
				displayText_unicode(51,17,".",&calibri26pt,1);
				displayText_unicode(20,17,"--",&calibri26pt,1);
				displayText_unicode(60,17,"-",&calibri26pt,1);
				displayText_unicode(80,17,"dB",&calibri26pt,1);
				displayText_unicode(7,7,"min:   --.-",&cambria_9ptA,1);
				displayText_unicode(70,7,"max:   --.-",&cambria_9ptA,1);
				min_val_aver=150;
				max_val_aver=20;
				//RTC_Reset_time();
			}
			else
			{
				rectangle (90,52,8,1,1);
				rectangle (91,53,6,1,1);
				rectangle (92,54,4,1,1);
				rectangle (93,55,2,1,1);
				sprintf(min_max_buff,"min: %3.1f",min_val_aver);
				displayText_unicode(7,7,min_max_buff,&cambria_9ptA,1);				
				sprintf(min_max_buff,"max: %3.1f",max_val_aver);
				displayText_unicode(70,7,min_max_buff,&cambria_9ptA,1);
				DisplayBar(1);
			}
				
				if (AUDIO_WEIGHTING_CURVE)
					displayText_unicode(103,50,"C eq",&cambria_9ptA,1);
				else
					displayText_unicode(102,50,"A eq",&cambria_9ptA,1);

				if(AVERAGE_TIME_REACHED)
				{
					rectangle (1,18,30,126,0);
					rectangle(1,2,12,125,0);
					
					for(i=0;i<40;i++)
					{
					buffer4[i]=0;
					}
					
					sprintf(buffer4,"%3.4f",DisplayLastDB(page_end_wsk, 1024));
					range_db_disp=DisplayLastDB(page_end_wsk, 1024);
					
					if(range_db_disp<100.000f)   /*	 Jesli wynik do wyswietlenia jest    */
					{
			
						for (i=0;i<2;i++)
						{
							dig[i]=buffer4[i];
						}
			
						frac[0] = buffer4[3];	  
						displayText_unicode(51,17,".",&calibri26pt,1);
						displayText_unicode(15,17,dig,&calibri26pt,1);
						displayText_unicode(60,17,frac,&calibri26pt,1);
						displayText_unicode(80,17,"dB",&calibri26pt,1);
					}	else{
			
						for (i=0;i<3;i++)
						{
						dig2[i]=buffer4[i];
						}
			
						frac2[0] = buffer4[4];	  
						displayText_unicode(61,17,".",&calibri26pt,1);
						displayText_unicode(8,17,dig2,&calibri26pt,1);
						displayText_unicode(70,17,frac2,&calibri26pt,1);
						displayText_unicode(90,17,"dB",&calibri26pt,1);
					}

	 
						sprintf(min_max_buff,"min: %3.1f",min_val_aver_mem);
						displayText_unicode(7,7,min_max_buff,&cambria_9ptA,1);				
						sprintf(min_max_buff,"max: %3.1f",max_val_aver_mem);
						displayText_unicode(70,7,min_max_buff,&cambria_9ptA,1);
				
				}
				
				RTC_TimeShow();

				
				
			} 
		}
	if(menuposition==2)
	{
		rectangle (1,52,11,28,0);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][20],&cambria9pt);
		displayText_unicode((128-len)/2,27,(void *)language_labels[language][20],&cambria9pt,1);  //STOP?
		
		if ((language==1) || (language==2)) len=113;
		else len=105;
		
		displayText_unicode(4,2,(void *)language_labels[language][18],&cambria9pt,1);   //NO
		displayText_unicode(len,2,(void *)language_labels[language][17],&cambria9pt,1);  // YES

	}
	
	if(menuposition==3)
	{
		rectangle (1,52,11,28,0);
		
		len=getStringLength_pixels_unicode((void *)warnings2[language][0],&cambria_9ptA);
		displayText_unicode((128-len)/2,49,(void *)warnings2[language][0],&cambria_9ptA,1);
		
		len=getStringLength_pixels_unicode((void *)warnings2[language][1],&cambria_9ptA);
		displayText_unicode((128-len)/2,34,(void *)warnings2[language][1],&cambria_9ptA,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][28],&cambria9pt);
		displayText_unicode((128-len)/2,20,(void *)language_labels[language][28],&cambria9pt,1);
		
		if ((language==1) || (language==2)) len=113;
		else len=105;
		
		displayText_unicode(4,2,(void *)language_labels[language][18],&cambria9pt,1);   //NO
		displayText_unicode(len,2,(void *)language_labels[language][17],&cambria9pt,1);  // YES


	}
	
	if(menuposition==5)
	{
		rectangle (1,52,11,28,0);

		len=getStringLength_pixels_unicode((void *)warnings2[language][0],&cambria_9ptA);
		displayText_unicode((128-len)/2,49,(void *)warnings2[language][0],&cambria_9ptA,1);
		
		len=getStringLength_pixels_unicode((void *)warnings2[language][1],&cambria_9ptA);
		displayText_unicode((128-len)/2,34,(void *)warnings2[language][1],&cambria_9ptA,1);	
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][28],&cambria9pt);
		displayText_unicode((128-len)/2,20,(void *)language_labels[language][28],&cambria9pt,1);
		
		if ((language==1) || (language==2)) len=113;
		else len=105;
			
		displayText_unicode(2,2,(void *)language_labels[language][18],&cambria9pt,1);   //NO
		displayText_unicode(len,2,(void *)language_labels[language][17],&cambria9pt,1);  // YES

	}	
			if(menuposition==6)			/*factory calibration   */
	{
		displayText_unicode(5,50,"Factory calibration",&cambria9pt,1);
		
		sprintf(buffer4,"%3.1fdB",artm_db_result_1000ms);
		displayText_unicode(15,17,buffer4,&calibri26pt,1);
			
		displayText_unicode(2,2,(void *)language_labels[language][21],&cambria9pt,1);
		displayText_unicode(85,2,(void *)language_labels[language][22],&cambria9pt,1);
		
		
		
	}
	
	
	
	
	
	if(menuposition==7)			/*set time average   */
	{
		rectangle (1,52,11,28,0);
		len=getStringLength_pixels_unicode((void *)language_labels[language][8],&cambria9pt);
		displayText_unicode((128-len)/2,49,(void *)language_labels[language][8],&cambria9pt,1);
		rectangle(45,48,1,37,1);
		rectangle(40,50,1,47,1);

		len=getStringLength_pixels_unicode((void *)language_labels[language][0],&cambria_9ptA);
		displayText_unicode(((128-len)/2)-7,32,"* ",&cambria_9ptA,1);
		displayText_unicode(128-((128-len)/2),32," *",&cambria_9ptA,1);
		displayText_unicode((128-len)/2,32,(void *)language_labels[language][0],&cambria_9ptA,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][1],&cambria_9ptA);
		displayText_unicode((128-len)/2,17,(void *)language_labels[language][1],&cambria_9ptA,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][2],&cambria_9ptA);
		displayText_unicode((128-len)/2,2,(void *)language_labels[language][2],&cambria_9ptA,1);
		

	}
	
		if(menuposition==8)		/*display results   */
	{
		rectangle (1,52,11,28,0);
		len=getStringLength_pixels_unicode((void *)language_labels[language][8],&cambria9pt);
		displayText_unicode((128-len)/2,49,(void *)language_labels[language][8],&cambria9pt,1);
		rectangle(45,48,1,37,1);
		rectangle(40,50,1,47,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][0],&cambria_9ptA);
		displayText_unicode((128-len)/2,32,(void *)language_labels[language][0],&cambria_9ptA,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][1],&cambria_9ptA);
		displayText_unicode(((128-len)/2)-7,17,"* ",&cambria_9ptA,1);
		displayText_unicode(128-((128-len)/2),17," *",&cambria_9ptA,1);
		displayText_unicode((128-len)/2,17,(void *)language_labels[language][1],&cambria_9ptA,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][2],&cambria_9ptA);
		displayText_unicode((128-len)/2,2,(void *)language_labels[language][2],&cambria_9ptA,1);
		

	}
	
	if(menuposition==9)				/*set backlit  */
	{
		rectangle (1,52,11,28,0);
		len=getStringLength_pixels_unicode((void *)language_labels[language][8],&cambria9pt);
		displayText_unicode((128-len)/2,49,(void *)language_labels[language][8],&cambria9pt,1);
		rectangle(45,48,1,37,1);
		rectangle(40,50,1,47,1);
		
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][0],&cambria_9ptA);
		displayText_unicode((128-len)/2,32,(void *)language_labels[language][0],&cambria_9ptA,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][1],&cambria_9ptA);
		displayText_unicode((128-len)/2,17,(void *)language_labels[language][1],&cambria_9ptA,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][2],&cambria_9ptA);
		displayText_unicode(((128-len)/2)-7,2,"* ",&cambria_9ptA,1);
		displayText_unicode(128-((128-len)/2),2," *",&cambria_9ptA,1);
		displayText_unicode((128-len)/2,2,(void *)language_labels[language][2],&cambria_9ptA,1);
		

	}
	
	if(menuposition==10)			/*set language  */
	{
		rectangle (1,52,11,28,0);
		len=getStringLength_pixels_unicode((void *)language_labels[language][8],&cambria9pt);
		displayText_unicode((128-len)/2,49,(void *)language_labels[language][8],&cambria9pt,1);
		rectangle(45,48,1,37,1);
		rectangle(40,50,1,47,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][3],&cambria_9ptA);
		displayText_unicode(((128-len)/2)-7,32,"* ",&cambria_9ptA,1);
		displayText_unicode(128-((128-len)/2),32," *",&cambria_9ptA,1);
		displayText_unicode((128-len)/2,32,(void *)language_labels[language][3],&cambria_9ptA,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][4],&cambria_9ptA);
		displayText_unicode((128-len)/2,17,(void *)language_labels[language][4],&cambria_9ptA,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][5],&cambria_9ptA);
		displayText_unicode((128-len)/2,2,(void *)language_labels[language][5],&cambria_9ptA,1);
		
		

	}
	
		if(menuposition==11)		/*calibration   */
	{
		rectangle (1,52,11,28,0);
		len=getStringLength_pixels_unicode((void *)language_labels[language][8],&cambria9pt);
		displayText_unicode((128-len)/2,49,(void *)language_labels[language][8],&cambria9pt,1);
		rectangle(45,48,1,37,1);
		rectangle(40,50,1,47,1);
		
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][3],&cambria_9ptA);
		displayText_unicode((128-len)/2,32,(void *)language_labels[language][3],&cambria_9ptA,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][4],&cambria_9ptA);
		displayText_unicode(((128-len)/2)-7,17,"* ",&cambria_9ptA,1);
		displayText_unicode(128-((128-len)/2),17," *",&cambria_9ptA,1);
		displayText_unicode((128-len)/2,17,(void *)language_labels[language][4],&cambria_9ptA,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][5],&cambria_9ptA);
		displayText_unicode((128-len)/2,2,(void *)language_labels[language][5],&cambria_9ptA,1);
		

	}
	
		if(menuposition==12)		/*sys info  */
	{
		rectangle (1,52,11,28,0);
		len=getStringLength_pixels_unicode((void *)language_labels[language][8],&cambria9pt);
		displayText_unicode((128-len)/2,49,(void *)language_labels[language][8],&cambria9pt,1);
		rectangle(45,48,1,37,1);
		rectangle(40,50,1,47,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][3],&cambria_9ptA);
		displayText_unicode((128-len)/2,32,(void *)language_labels[language][3],&cambria_9ptA,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][4],&cambria_9ptA);
		displayText_unicode((128-len)/2,17,(void *)language_labels[language][4],&cambria_9ptA,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][5],&cambria_9ptA);
		displayText_unicode(((128-len)/2)-7,2,"* ",&cambria_9ptA,1);
		displayText_unicode(128-((128-len)/2),2," *",&cambria_9ptA,1);
		displayText_unicode((128-len)/2,2,(void *)language_labels[language][5],&cambria_9ptA,1);
		
		

	}
	
	if(menuposition==13)  /*set default settings   */
	{
		rectangle (1,52,11,28,0);
		len=getStringLength_pixels_unicode((void *)language_labels[language][8],&cambria9pt);
		displayText_unicode((128-len)/2,49,(void *)language_labels[language][8],&cambria9pt,1);
		rectangle(45,48,1,37,1);
		rectangle(40,50,1,47,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][6],&cambria_9ptA);
		displayText_unicode(((128-len)/2)-7,32,"* ",&cambria_9ptA,1);
		displayText_unicode(128-((128-len)/2),32," *",&cambria_9ptA,1);
		displayText_unicode((128-len)/2,32,(void *)language_labels[language][6],&cambria_9ptA,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][7],&cambria_9ptA);
		displayText_unicode((128-len)/2,17,(void *)language_labels[language][7],&cambria_9ptA,1);
		

	}
	
	if(menuposition==14)  /*exit  */
	{
		rectangle (1,52,11,28,0);
		len=getStringLength_pixels_unicode((void *)language_labels[language][8],&cambria9pt);
		displayText_unicode((128-len)/2,49,(void *)language_labels[language][8],&cambria9pt,1);
		rectangle(45,48,1,37,1);
		rectangle(40,50,1,47,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][6],&cambria_9ptA);
		displayText_unicode((128-len)/2,32,(void *)language_labels[language][6],&cambria_9ptA,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][7],&cambria_9ptA);
		displayText_unicode(((128-len)/2)-7,17,"* ",&cambria_9ptA,1);
		displayText_unicode(128-((128-len)/2),17," *",&cambria_9ptA,1);
		displayText_unicode((128-len)/2,17,(void *)language_labels[language][7],&cambria_9ptA,1);
		
		

	}
	
	
	
	
	if(menuposition==15)   //ustawienie podswietlenia
	{
		rectangle (1,52,11,28,0);
		len=getStringLength_pixels_unicode((void *)language_labels[language][2],&cambria9pt);		
		displayText_unicode((128-len)/2,40,(void *)language_labels[language][2],&cambria9pt,1);
		

		backlite_1=(TIM1->CCR4)/10;

		for(uint8_t n=5; n<120; n+=12)
		{
			rectangle(n,15,9,9,1);
		}
		
		if(backlite_1==10)rectangle(114,16,7,7,0);
		else if(backlite_1==9)rectangle(102,16,7,7,0);
		else if(backlite_1==8)rectangle(90,16,7,7,0);
		else if(backlite_1==7)rectangle(78,16,7,7,0);
		else if(backlite_1==6)rectangle(66,16,7,7,0);
		else if(backlite_1==5)rectangle(54,16,7,7,0);
		else if(backlite_1==4)rectangle(42,16,7,7,0);
		else if(backlite_1==3)rectangle(30,16,7,7,0);
		else if(backlite_1==2)rectangle(18,16,7,7,0);
		else if(backlite_1==1)rectangle(6,16,7,7,0);
	}
	
	if(menuposition==16)
	{
		rectangle (1,52,11,28,0);
		sprintf(bkl,"%02d",hour_time_aver);
		sprintf(bkl2,"%02d",min_time_aver);
		sprintf(bkl3,"%02d",sec_time_aver);

		displayText_unicode(2,5,bkl,&calibri26pt,1);
		displayText_unicode(39,7,":",&calibri26pt,1);
		displayText_unicode(46,5,bkl2,&calibri26pt,1);
		displayText_unicode(83,7,":",&calibri26pt,1);
		displayText_unicode(90,5,bkl3,&calibri26pt,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][9],&cambria_9ptA);		//SET AVERAGE TIME
		displayText_unicode((128-len)/2,50,(void *)language_labels[language][9],&cambria_9ptA,1);
		
		displayText_unicode(12,33,"hh",&cambria9pt,1);
		displayText_unicode(52,33,"mm",&cambria9pt,1);	
		displayText_unicode(103,33,"ss",&cambria9pt,1);	
		

		
		if(SET_HOURS_TIME_AVERAGE) 
		{
			rectangle(3,6,1,30,1);
			rectangle(8,3,1,20,1);
			rectangle(13,0,1,10,1);
		}
		
		if(SET_MINUTES_TIME_AVERAGE) 
		{
			rectangle(47,6,1,30,1);
			rectangle(52,3,1,20,1);
			rectangle(57,0,1,10,1);
		}
		
		if(SET_SECONDS_TIME_AVERAGE) 
		{
			rectangle(92,6,1,30,1);
			rectangle(97,3,1,20,1);
			rectangle(102,0,1,10,1);
		}
	
}
		
	
	if(menuposition==17)//RESULTS LIST
	{
		rectangle (1,50,13,125,0);
		len=getStringLength_pixels_unicode((void *)language_labels[language][10],&cambria_9ptA);
		displayText_unicode((128-len)/2,50,(void *)language_labels[language][10],&cambria_9ptA,1);	
		DisplayListDb(page_end_wsk, 1024);
	}
	
	if(menuposition==18)			/*set language  */
	{
		rectangle (1,52,11,28,0);
		if(language==0)
		{			
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][3],&cambria9pt);
		displayText_unicode((128-len)/2,49,(void *)language_labels[language][3],&cambria9pt,1);	

		len=getStringLength_pixels_unicode((void *)language_labels[0][19],&cambria_9ptA);
		displayText_unicode(((128-len)/2)-7,32,"* ",&cambria_9ptA,1);
		displayText_unicode(128-((128-len)/2),32," *",&cambria_9ptA,1);
		displayText_unicode((128-len)/2,32,(void *)language_labels[0][19],&cambria_9ptA,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[1][19],&cambria_9ptA);
		displayText_unicode((128-len)/2,17,(void *)language_labels[1][19],&cambria_9ptA,1);
		
		len=getStringLength_pixels_unicode((void *)language_labels[2][19],&cambria_9ptA);
		displayText_unicode((128-len)/2,2,(void *)language_labels[2][19],&cambria_9ptA,1);
		

		}
		if(language==1)
		{
			len=getStringLength_pixels_unicode((void *)language_labels[language][3],&cambria9pt);
			displayText_unicode((128-len)/2,49,(void *)language_labels[language][3],&cambria9pt,1);	
			
		//	displayText_unicode(42,49,"Sprache",&cambria9pt,1);
			
			len=getStringLength_pixels_unicode((void *)language_labels[0][19],&cambria_9ptA);
			displayText_unicode((128-len)/2,32,(void *)language_labels[0][19],&cambria_9ptA,1);
		
			len=getStringLength_pixels_unicode((void *)language_labels[1][19],&cambria_9ptA);
			displayText_unicode(((128-len)/2)-7,17,"* ",&cambria_9ptA,1);
			displayText_unicode(128-((128-len)/2),17," *",&cambria_9ptA,1);
			displayText_unicode((128-len)/2,17,(void *)language_labels[1][19],&cambria_9ptA,1);
		
			len=getStringLength_pixels_unicode((void *)language_labels[2][19],&cambria_9ptA);
			displayText_unicode((128-len)/2,2,(void *)language_labels[2][19],&cambria_9ptA,1);
			
			

		}
		if(language==2)
		{
			len=getStringLength_pixels_unicode((void *)language_labels[language][3],&cambria9pt);
			displayText_unicode((128-len)/2,49,(void *)language_labels[language][3],&cambria9pt,1);	
			
		//	displayText_unicode(48,49,"Taal",&cambria9pt,1);
			
			len=getStringLength_pixels_unicode((void *)language_labels[0][19],&cambria_9ptA);
			displayText_unicode((128-len)/2,32,(void *)language_labels[0][19],&cambria_9ptA,1);
		
			len=getStringLength_pixels_unicode((void *)language_labels[1][19],&cambria_9ptA);
			displayText_unicode((128-len)/2,17,(void *)language_labels[1][19],&cambria_9ptA,1);
		
			len=getStringLength_pixels_unicode((void *)language_labels[2][19],&cambria_9ptA);
			displayText_unicode(((128-len)/2)-7,2,"* ",&cambria_9ptA,1);
			displayText_unicode(128-((128-len)/2),2," *",&cambria_9ptA,1);
			displayText_unicode((128-len)/2,2,(void *)language_labels[2][19],&cambria_9ptA,1);
			

		}
	}

		if(menuposition==19)			/*user calibration  */
	{

		len=getStringLength_pixels_unicode((void *)language_labels[language][14],&cambria_9ptA);
		displayText_unicode((128-len)/2,49,(void *)language_labels[language][14],&cambria_9ptA,1);
		

		if(TYPE_OF_CALIBRATION_DIS)
		{	
			if(CHOOSEN_USER_CALIBRATION)
			{
				
				displayText_unicode(1,2,(void *)language_labels[language][12],&cambria_9ptA,1); //CURRENT
				if(language==2)len=86;
				else if(language==1)len=104;
				else len=100;
				displayText_unicode(len,2,(void *)language_labels[language][11],&cambria_9ptA,1); //NEW
				
//				displayText_unicode(1,2,"CURRENT",&cambria9pt,1);
//				displayText_unicode(100,2,"NEW",&cambria9pt,1);
			}else
				{
					len=getStringLength_pixels_unicode((void *)language_labels[language][15],&cambria_9ptA);
					displayText_unicode((128-len)/2,30,(void *)language_labels[language][15],&cambria_9ptA,1);
		
					len=getStringLength_pixels_unicode((void *)language_labels[language][16],&cambria_9ptA);
					displayText_unicode(((128-len)/2)-7,10,"* ",&cambria_9ptA,1);
					displayText_unicode(128-((128-len)/2),10," *",&cambria_9ptA,1);
					displayText_unicode((128-len)/2,10,(void *)language_labels[language][16],&cambria_9ptA,1);		
					
					
//			displayText_unicode(14,30,"Factory calibration",&cambria_9ptA,1);
//			displayText_unicode(15,10,"*User Calibration*",&cambria_9ptA,1);
				}
		
		}else
			{
				len=getStringLength_pixels_unicode((void *)language_labels[language][15],&cambria_9ptA);
				displayText_unicode(((128-len)/2)-7,30,"* ",&cambria_9ptA,1);
				displayText_unicode(128-((128-len)/2),30," *",&cambria_9ptA,1);
				displayText_unicode((128-len)/2,30,(void *)language_labels[language][15],&cambria_9ptA,1);
		
				len=getStringLength_pixels_unicode((void *)language_labels[language][16],&cambria_9ptA);
				displayText_unicode((128-len)/2,10,(void *)language_labels[language][16],&cambria_9ptA,1);
				
				
				
//				displayText_unicode(8,30,"*Factory calibration*",&cambria_9ptA,1);
//				displayText_unicode(21,10,"User Calibration",&cambria_9ptA,1);
			}
	}
	
	if(menuposition==20)   //display system information
	{
		rectangle (1,52,11,28,0);
		len=getStringLength_pixels_unicode((void *)language_labels[language][5],&cambria_9ptA);
		displayText_unicode((128-len)/2,50,(void *)language_labels[language][5],&cambria_9ptA,1);
		
		displayText_unicode(1,37,(void *)language_labels[language][24],&cambria_9ptA,1);
		displayText_unicode(1,25,(void *)language_labels[language][25],&cambria_9ptA,1);
		displayText_unicode(1,13,(void *)language_labels[language][26],&cambria_9ptA,1);
		displayText_unicode(1,1,(void *)language_labels[language][27],&cambria_9ptA,1);
		
		DisplayVerDate(dateandfirmware,60,25,60,37);
		sprintf(buffer4,"%09d",total_turn_on);	
		displayText_unicode(60,13,buffer4,&cambria_9ptA,1);
		sprintf(buffer4,"%09d",time_one_on);	
		displayText_unicode(60,1,buffer4,&cambria_9ptA,1);
	}
	
	
	
	if(menuposition==21)  //Reset to default  setting?
	{
		len=getStringLength_pixels_unicode((void *)warnings3[language][0],&cambria9pt);
		displayText_unicode((128-len)/2,49,(void *)warnings3[language][0],&cambria9pt,1);
		
		len=getStringLength_pixels_unicode((void *)warnings3[language][1],&cambria9pt);
		displayText_unicode((128-len)/2,34,(void *)warnings3[language][1],&cambria9pt,1);
		
		len=getStringLength_pixels_unicode((void *)warnings3[language][2],&cambria9pt);
		displayText_unicode((128-len)/2,19,(void *)warnings3[language][2],&cambria9pt,1);
		
		
		if ((language==1) || (language==2)) len=113;
		else len=105;
		displayText_unicode(4,2,(void *)language_labels[language][18],&cambria9pt,1);   //NO
		displayText_unicode(len,2,(void *)language_labels[language][17],&cambria9pt,1);  // YES
		
//		displayText_unicode(4,2,"NO",&cambria9pt,1);
//		displayText_unicode(100,2,"YES",&cambria9pt,1);
		
	}
	
	
	if(menuposition==22)
	{
		len=getStringLength_pixels_unicode((void *)warnings[language][0],&cambria9pt);
		displayText_unicode((128-len)/2,49,(void *)warnings[language][0],&cambria9pt,1);
		
		len=getStringLength_pixels_unicode((void *)warnings[language][1],&cambria9pt);
		displayText_unicode((128-len)/2,34,(void *)warnings[language][1],&cambria9pt,1);
		
		len=getStringLength_pixels_unicode((void *)warnings[language][2],&cambria9pt);
		displayText_unicode((128-len)/2,19,(void *)warnings[language][2],&cambria9pt,1);
		
		len=getStringLength_pixels_unicode((void *)warnings[language][3],&cambria9pt);
		displayText_unicode((128-len)/2,4,(void *)warnings[language][3],&cambria9pt,1);

		
	}
	
	if(menuposition==23)
	{
		rectangle (1,50,13,125,0);
		
		
		
		len=getStringLength_pixels_unicode((void *)language_labels[language][23],&cambria_9ptA);
		displayText_unicode((128-len)/2,49,(void *)language_labels[language][23],&cambria_9ptA,1);//calibration
		
		sprintf(buffer4,"%3.1fdB",artm_db_result_1000ms);
		displayText_unicode(15,17,buffer4,&calibri26pt,1);
			
		displayText_unicode(2,2,(void *)language_labels[language][21],&cambria9pt,1);
		displayText_unicode(86,2,(void *)language_labels[language][22],&cambria9pt,1);
		
		

		
	}
	
	
	
	lcd_updatewholedisplay(0);

}




void DisplayShortMessage(void)
{
	static uint8_t tick;
	
	if(menuposition==30)
	{
		tick++;
		if(tick>2) 
		{
			menuposition=1;
			tick=0;
		}
		
	}
}
