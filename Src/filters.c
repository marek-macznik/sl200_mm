

#include "stm32l4xx_hal.h"
#include "arm_math.h"
#include "dfsdm.h"
#include "system.h"
#include "buttons.h"
#include "display.h"



#define SaturaLH(N, L, H) (((N)<(L))?(L):(((N)>(H))?(H):(N)))
#define DMA_LENGTH         (uint32_t)2048
#define TEST_LENGTH_SAMPLES (DMA_LENGTH/2)
#define BLOCK_SIZE		256
#define NUM_TAPS		165
#define NUM_TAPS2		61
#define IIR_BLOCK_SIZE		16


#define PACKET_SIZE (uint32_t)(1024)

volatile float artm_db_result_125ms;  //fast
volatile float artm_db_result_1000ms; //slow
volatile float artm_db_result_inf;    //inf
volatile float rmsa4 = 1.f;

volatile float artm_db_result_1000ms_2; //slow
volatile float artm_db_result_125ms_2;  //fast

volatile float db_result;
float calibration_value2 = 1.f;
float calibration_value3;

double SWDB(double w, double db_old, double db_new);

uint32_t czas1;
uint32_t czas_wyk1;

#define A_WEIGHT_LEN		7
#define C_WEIGHT_LEN		5
#define MATLAB_COEFFS		1
q31_t                      	 RecBuff[2048];
q31_t                      	 PlayBuff[2048];
float												 PlayBuff_float[2048];
float 											 Filtered[2048];
float 											 ax_nx[A_WEIGHT_LEN];
float 											 ay_nx[A_WEIGHT_LEN];
float 											 cx_nx[C_WEIGHT_LEN];
float 											 cy_nx[C_WEIGHT_LEN];

	
float A_weightenings_NOM[7] = {
#ifdef MATLAB_COEFFS
#warning "MATLAB COEFFS SELECTED"
	0.169995f,
	0.280415f,
	-1.120575f,
	0.131563f,
	0.974154f,
	-0.282741f,
	-0.152811f,
#else
#warning "COEFFS FROM INTERNET"
	 0.169994948147430f,
	 0.280415310498794f,
	-1.120574766348363f,
	 0.131562559965936f,
	 0.974153561246036f,
	-0.282740857326553f,
	-0.152810756202003f
#endif	
};


float A_weightenings_DEN[7] = {
#ifdef MATLAB_COEFFS	
	1.000000f,
	-2.129794f,
	0.429961f,
	1.621327f,
	-0.966700f,
	0.001210f,
	0.044003f,
#else
   1.00000000000000000f,
  -2.12979364760736134f,
   0.42996125885751674f,
   1.62132698199721426f,
  -0.96669962900852902f,
   0.00121015844426781f,
   0.04400300696788968f
#endif
};


float C_weightenings_NOM[5] = {
 0.197948f,
 0.000000f,
 -0.395896f,
 0.000000f,
 0.197948f,
};


float C_weightenings_DEN[5] = {
 1.000000f,
 -2.218704f,
 1.454149f,
 -0.247926f,
 0.012487f,
};

void IIR_FILTER(float *input, float *output, float *in_state, float *out_state, uint32_t blockSize, float *B, float *A, uint32_t filterLen);
float Integral_Trap(float *input, float sample_rate, uint32_t blockSize);

/*************************************************************************************************************************/
/************************************ PIERWSZA POLOWA BUFORU PROBEK Z MIKROFONU ******************************************/
/*************************************************************************************************************************/		
void HAL_DFSDM_FilterRegConvHalfCpltCallback(DFSDM_Filter_HandleTypeDef *hdfsdm1_filter)
{
	if (hdfsdm1_filter->Instance==hdfsdm1_filter0.Instance)
	{
		arm_copy_q31(&RecBuff[0], &PlayBuff[0], PACKET_SIZE);	 
		arm_q31_to_float(&PlayBuff[0], &PlayBuff_float[0], PACKET_SIZE);
	/*************************************************************************************************************************/
	/************************************ FILTRY DO KRZYWYCH KOREKCYJNYCH A LUB C  POCZATEK **********************************/
	/*************************************************************************************************************************/			
		if (AUDIO_WEIGHTING_CURVE) // C weighteing
		{																												
			IIR_FILTER(&PlayBuff_float[0], &Filtered[0], cx_nx, cy_nx, PACKET_SIZE, C_weightenings_NOM, C_weightenings_DEN, C_WEIGHT_LEN);
		}
		else // A weighteing
		{
			IIR_FILTER(&PlayBuff_float[0], &Filtered[0], ax_nx, ay_nx, PACKET_SIZE, A_weightenings_NOM, A_weightenings_DEN, A_WEIGHT_LEN);
		}
	/*************************************************************************************************************************/
	/************************************ FILTRY DO KRZYWYCH KOREKCYJNYCH A LUB C  KONIEC **********************************/
	/*************************************************************************************************************************/		
	}
}
/*************************************************************************************************************************/
/************************************ DRUGA POLOWA BUFORU PROBEK Z MIKROFONU ******************************************/
/*************************************************************************************************************************/	

float Filtered_Log[2048];
void HAL_DFSDM_FilterRegConvCpltCallback(DFSDM_Filter_HandleTypeDef *hdfsdm1_filter)
{
	if (hdfsdm1_filter->Instance==hdfsdm1_filter0.Instance){
		static double licznik_125ms;
		static double licznik_1000ms;
		static double licznik_inf;	
		static float test_db_result;
		float correction;
		
		arm_copy_q31(&RecBuff[1024], &PlayBuff[1024], PACKET_SIZE); 
		arm_q31_to_float(&PlayBuff[1024], &PlayBuff_float[1024], PACKET_SIZE);
			
/*************************************************************************************************************************/
/************************************ FILTRY DO KRZYWYCH KOREKCYJNYCH A LUB C  POCZATEK **********************************/
/*************************************************************************************************************************/			
			if (AUDIO_WEIGHTING_CURVE) // C weighteing
			{																												
				IIR_FILTER(&PlayBuff_float[1024], &Filtered[1024], cx_nx, cy_nx, PACKET_SIZE, C_weightenings_NOM, C_weightenings_DEN, C_WEIGHT_LEN);
			}
			else // A weighteing
			{
				IIR_FILTER(&PlayBuff_float[1024], &Filtered[1024], ax_nx, ay_nx, PACKET_SIZE, A_weightenings_NOM, A_weightenings_DEN, A_WEIGHT_LEN);
			}
/*************************************************************************************************************************/
/************************************ FILTRY DO KRZYWYCH KOREKCYJNYCH A LUB C  KONIEC **********************************/
/*************************************************************************************************************************/					

			rmsa4 = Integral_Trap(Filtered, 48000.f, 2048);
			//arm_rms_f32(Filtered, 2048, &rmsa4);
			test_db_result = 20.f * log10f (rmsa4 / calibration_value2) + 94.f;
			
		//************ mala korekcja wyniku POCZATEK *********************
		if (test_db_result < 94.f) correction =21.f;
		if (test_db_result >=94.f) correction =19.5f;
		db_result = (correction * log10f(rmsa4/calibration_value2))+94;
		//************ mala korekcja wyniku KONIEC ***********************	
		
		if (CLEAR_MIN_MAX_AVERAGE)   // czysc zmienne odpowiedzalne za liczenie usredniania wyniku
		{
			licznik_inf=0;
			licznik_1000ms=0;
			parameters &= ~(0x08);
		}
		
/*************************************************************************************************************************/
/************************************ USREDNIANIE 3 KOLEJNYCH OKRES�W  42.67ms * 3 = 128ms POCZATEK **********************/
/*************************************************************************************************************************/	
		artm_db_result_125ms = SWDB(licznik_125ms,artm_db_result_125ms, db_result);
		licznik_125ms++;
		if (licznik_125ms >=4)     
		{
			artm_db_result_125ms_2=artm_db_result_125ms;           //ZMENNA PRZECHOWUJACA WYNIK W DECYBELACH Z OKRESU CZASU OKOLO 128ms
			licznik_125ms=0;   // IRQ every 42,66ms*3   128ms
		}	
/*************************************************************************************************************************/
/************************************ USREDNIANIE 3 KOLEJNYCH OKRES�W  42.67ms * 3 = 128ms KONIEC ************************/
/*************************************************************************************************************************/					
		
/*************************************************************************************************************************/
/************************************ USREDNIANIE 23 KOLEJNYCH OKRES�W  42.67ms * 23 = 981ms POCZATEK **********************/
/*************************************************************************************************************************/			
		artm_db_result_1000ms = SWDB(licznik_1000ms,artm_db_result_1000ms, db_result);
		licznik_1000ms++;
		if (licznik_1000ms >=24){    // IRQ every 42,66ms*23   981ms
			licznik_1000ms=0;
			artm_db_result_1000ms_2 = artm_db_result_1000ms;    //ZMENNA PRZECHOWUJACA WYNIK W DECYBELACH Z OKRESU CZASU OKOLO 128ms     
			artm_db_result_inf = SWDB(licznik_inf,artm_db_result_inf, artm_db_result_1000ms); //ZMENNA PRZECHOWUJACA WYNIK W DECYBELACH Z OKRESU CZASU od 1s do 10 godzin 
			licznik_inf++;
			parameters |= 0x40;   
		}
/*************************************************************************************************************************/
/************************************ USREDNIANIE 3 KOLEJNYCH OKRES�W  42.67ms * 3 = 128ms KONIEC ************************/
/*************************************************************************************************************************/			

	if (min_val > artm_db_result_125ms) min_val = artm_db_result_125ms;       // min indykator na wyswietlaczu  (ekran glowny)
	if (max_val < artm_db_result_125ms) max_val = artm_db_result_125ms;       // max indykator na wyswietlaczu  (ekran glowny)	

	if (min_val_aver > artm_db_result_125ms) min_val_aver = artm_db_result_125ms;   // min indykator na wyswietlaczu  (ekran usreniania)
	if (max_val_aver < artm_db_result_125ms) max_val_aver = artm_db_result_125ms;   // max indykator na wyswietlaczu  (ekran usredniania)				
	}
}

	
void FiltersInit(void)
{
	HAL_DFSDM_FilterRegularStart_DMA(&hdfsdm1_filter0, RecBuff, DMA_LENGTH);
	for(uint8_t i = 0; i < A_WEIGHT_LEN; i++)
	{
		ax_nx[i] = .0f; 
		ay_nx[i] = .0f;
	}
	for(uint8_t i = 0; i < C_WEIGHT_LEN; i++)
	{
		cx_nx[i] = .0f; 
		cy_nx[i] = .0f;
	}
}
	
	
double SWDB(double w, double db_old, double db_new)
{
	double in;
	double out;
	double sum;
	
	in= (pow(10,(db_old/10)))*w;
	out= pow(10,(db_new/10));
	sum= (in+out) / (1+w);
	
  return (10 * log10(sum));
}


void IIR_FILTER(float *input, float *output, float *in_state, float *out_state, uint32_t blockSize, float *B, float *A, uint32_t filterLen)
{
    float v_n = .0f;
    for(int i = 0; i < blockSize; i++)
    {
        for(int k = filterLen-1; k > 0; k--)
        {
            in_state[k] = in_state[k-1];
            out_state[k] = out_state[k-1];
        }
        in_state[0] = input[i];
        v_n = 0.0f;

        for(int k = 0; k < filterLen; k++)
        {
            v_n += B[k] * in_state[k];
        }
        out_state[0] = v_n * A[0];
        for(int k = 1; k < filterLen; k++)
        {
            out_state[0] = out_state[0] - A[k] * out_state[k];
        }
        output[i] = out_state[0];
    }
}

float Integral_Trap(float *input, float sample_rate, uint32_t blockSize)
{
	float sum = .0f;
	for (uint32_t i = 1; i < blockSize; i++)
	{
		sum += 1.f/sample_rate * fabsf( (input[i] + input[i-1])) / 2.f;
	}
	return (sum * blockSize);
}
