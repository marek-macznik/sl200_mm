#include "stm32l4xx_hal.h"
#include "arm_math.h"
#include "lcdst7565r_unicode.h"
#include "system.h"
#include "memory.h"
#include "display.h"
#include "iwdg.h"
#include "dfsdm.h"

uint32_t language;
uint32_t backlit;
uint8_t menuposition;
uint8_t menuposition_mem;
uint16_t parameters;
uint32_t total_turn_on;
uint32_t time_one_on;
uint32_t time_to_off;
extern uint32_t voltage;
uint16_t bat_time;
extern uint32_t temp_sumtime;


void ResetTimeOff(void);
void battery_check(uint8_t menu, uint8_t now);
void AutoTurnOff(uint8_t now, uint8_t lowbat);
void AutoBacklightOff(void);
void ConvertSecondsToHMS(uint32_t time_s);
void ResetRtc(void);
uint32_t GetSecondsRtc(void);
//HAL_TIM_IC_CaptureCallback(&htim16)
//{


//}


void ConvertSecondsToHMS(uint32_t time_s)
{
	hour_time_aver = time_s / 3600;
	min_time_aver = (time_s - 3600 * hour_time_aver) / 60;
	sec_time_aver = time_s - 3600 * hour_time_aver - 60 * min_time_aver;
}




// 
void AutoBacklightOff(void)	
{
	static uint8_t mem_pwm;
	
	
	if(TIM2->CNT>3800000 && mem_pwm==0)//6min 20sec  3800000
	{
		TIM1->CCR4 = 0;   //backlight off
		mem_pwm = 1;
	}
			
	if (TIM2->CNT<=3800000 && mem_pwm==1)
	{
		if (CheckFreeSpace(page_lang_backlit, 512)<512)
		{
			TIM1->CCR4 = ReadFirstOrSecondValFromFlash(page_lang_backlit,512,0);
			if(TIM1->CCR4 == 0)
			{
				TIM1->CCR4 = 100;
			}
		}else TIM1->CCR4=100;
		mem_pwm =0;
	}
}




// 1 param   ->    1-switch off now    or 0  to switch of after some time
// 2 param   ->    1- display low bat logo during switching off the meter,   
void AutoTurnOff(uint8_t now, uint8_t lowbat)
{
	uint32_t sumtime;
	if(time_to_off>360 || now==1)
	{
		HAL_DFSDM_FilterRegularStop_DMA(&hdfsdm1_filter0);
		if(CheckFreeSpace(page_on_counter,512)<=10) erase_page_bank(ERASE_PAGE_ON_SECON);
		save_two_int_to_flash(page_on_counter,total_turn_on,time_one_on);
		
		sumtime = hour_time_aver*3600+min_time_aver*60+sec_time_aver;
		if(sumtime != ReadFirstOrSecondValFromFlash(page_time_average,512,0))
		{
		
		if(CheckFreeSpace(page_time_average,512)<=10) erase_page_bank(ERASE_PAGE_US_FA_SUMTIME);
		
		if(TYPE_OF_CALIBRATION_DIS)save_two_int_to_flash(page_time_average,1,sumtime);
		else save_two_int_to_flash(page_time_average,0,sumtime);
		}
			
		
		clear_lcd();
		lcd_updatewholedisplay(0);
	  _delay_ms(100);
		if(lowbat==1)
		{
			displayText_unicode(16,2,LOW_BAT,&icon,1);
			lcd_updatewholedisplay(0);
	  	HAL_Delay(200);
			HAL_IWDG_Refresh(&hiwdg);
			HAL_Delay(200);
			HAL_IWDG_Refresh(&hiwdg);
			HAL_Delay(200);
			HAL_IWDG_Refresh(&hiwdg);
			HAL_Delay(200);
			HAL_IWDG_Refresh(&hiwdg);
			clear_lcd();
	  }
		displayText_unicode(40,8,power_icon,&icon,1);
    lcd_updatewholedisplay(0);
	  				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);
				HAL_Delay(200);
				HAL_IWDG_Refresh(&hiwdg);

		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
	}

}

void ResetTimeOff(void)
{
	TIM2->CNT = 0;
	if(!(DISPLAY_TIME_AVERAGE)) time_to_off=0;
}

void battery_check (uint8_t menu, uint8_t now){

	char buffer[20];
	bat_time++;
	if(bat_time==5 || now==1)
	{
		if(voltage<500)voltage=2600;
		if(voltage >= 3300){
			if(menu==0 || menu==1){
			sprintf(buffer,"%u",small_battery_icon+3);
			displayText_unicode(5,52,buffer,&icon,1);
			lcd_updatewholedisplay(0);
			}
		}else if((voltage < 3300) && (voltage >= 3000)){
			if(menu==0 || menu==1){
			sprintf(buffer,"%u",small_battery_icon+2);
			displayText_unicode(5,52,buffer,&icon,1);
			lcd_updatewholedisplay(0);
			}
		}else if((voltage < 3000) && (voltage >= 2700)){
			if(menu==0 || menu==1){
			sprintf(buffer,"%u",small_battery_icon+1);
			displayText_unicode(5,52,buffer,&icon,1);
			lcd_updatewholedisplay(0);
			}
		}else if((voltage < 2700) && (voltage >= 2400)){
			if(menu==0 || menu==1){
			sprintf(buffer,"%u",small_battery_icon);
			displayText_unicode(5,52,buffer,&icon,1);
			lcd_updatewholedisplay(0);
			}
		}else if(voltage < 2450){
			AutoTurnOff(1,1);   //if battery is on critical low level, switch off meter 
		
		}
		
		
	}
	if(bat_time==1000) bat_time=0;
}




void ResetRtc(void)
{

	PWR->CR1 |= (1<<8);		//ENABLE rtc registers to write

	RTC->WPR  = 0xCA;			//WRITE key to enable rtc
	RTC->WPR  = 0x53;			//WRITE key to enable rtc


	RTC->ISR |= (1<<7);


	while ((RTC->ISR & RTC_ISR_INITF) == 0)
		{
	
		}
	RTC->TR &= ~(0x003F7F7F);
	
	RTC->ISR &= ~(1<<7);	
	PWR->CR1 &= ~(1<<8);	

}

uint32_t GetSecondsRtc(void)
{
	uint32_t total_sec_rtc;
	uint32_t tmpreg = 0;
	uint32_t tmp=0;
	uint8_t hours;
	uint8_t minutes;
	uint8_t seconds;
	
	tmpreg = (uint32_t)(RTC->TR & ((uint32_t)0x007F7F7F));
	
	hours = (uint8_t)((tmpreg & (RTC_TR_HT | RTC_TR_HU)) >> 16);
  minutes = (uint8_t)((tmpreg & (RTC_TR_MNT | RTC_TR_MNU)) >>8);
  seconds = (uint8_t)(tmpreg & (RTC_TR_ST | RTC_TR_SU));

	tmp = ((uint8_t)(hours & (uint8_t)0xF0) >> (uint8_t)0x4) * 10;
	hours = (tmp + (hours & (uint8_t)0x0F));
	
	tmp = ((uint8_t)(minutes & (uint8_t)0xF0) >> (uint8_t)0x4) * 10;
	minutes = (tmp + (minutes & (uint8_t)0x0F));
	
	tmp = ((uint8_t)(seconds & (uint8_t)0xF0) >> (uint8_t)0x4) * 10;
	seconds = (tmp + (seconds & (uint8_t)0x0F));
  
	total_sec_rtc = hours * 3600 + minutes * 60 + seconds;
 
	return (total_sec_rtc);
}


