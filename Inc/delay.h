



#ifndef _UTIL_DELAY_H_
#define _UTIL_DELAY_H_ 1



#define F_CPU 80000000
void _delay_ms( double __ms);
__inline void _delayInline_ms( double __ms)
{
	double __tmp = ((F_CPU) / 162e3) * __ms;
    while (__tmp>0) __tmp--;
}


#endif /* _UTIL_DELAY_H_ */
