#ifndef __MEMORY_H
#define __MEMORY_H




#include "stm32l4xx_hal.h"
#include "arm_math.h"

#define	ERASE_PAGE_DATA_1									(uint16_t)59
#define	ERASE_PAGE_DATA_2									(uint16_t)60
#define	ERASE_PAGE_LANG_BACKLIGHT					(uint16_t)62
#define	ERASE_PAGE_ON_SECON								(uint16_t)58
#define	ERASE_PAGE_DATE_FIRMWARE					(uint16_t)63
#define	ERASE_PAGE_USER_CALIBRATION				(uint16_t)57
#define	ERASE_PAGE_US_FA_SUMTIME					(uint16_t)61
#define	ERASE_PAGE_FACTORY_CALIBRATION		(uint16_t)56



extern const uint32_t *page_end_wsk;  				/*	256 KB Flash module		*/
extern const uint64_t *page_end_wsk_double;		/*	256 KB Flash module		*/
extern const uint64_t *beginlistspace;				/*	256 KB Flash module		*/
extern const uint32_t *page_lang_backlit;
extern const uint32_t *page_on_counter;
extern const uint8_t  *dateandfirmware;
extern const uint32_t *page_user_calib;  					
extern const uint32_t *page_time_average;  				
extern const uint32_t *page_factory_calib;
extern uint16_t errasefactorycalibration;






struct skladowe_dane {
    float part1;
		uint32_t part2;
	};

struct link_two_variables{
	uint32_t part1;
	uint32_t part2;
};

void erase_page(uint16_t page);
void erase_page_bank(uint16_t page);
uint16_t save_data_to_flash(const uint32_t *adr_pg, float a, uint32_t b, uint16_t sizee);	
uint16_t CheckFreeSpace(const uint32_t *adr_pg, uint16_t space);	
void	DisplayListDb(const uint32_t *adr_pg, uint16_t space);
void CopyLastTenResults(const uint64_t *adr_pg, const uint64_t *adr_beging_page);
uint16_t save_two_int_to_flash(const uint32_t *adr_pg, uint32_t a, uint32_t b);
uint32_t ReadFirstOrSecondValFromFlash(const uint32_t *adr_pg, uint16_t space, uint8_t first_second);
float DisplayLastDB(const uint32_t *adr_pg, uint16_t space);
void DisplayVerDate(const uint8_t *adr_pg, uint8_t xdate, uint8_t ydate, uint8_t xfirmware, uint8_t yfirmware);
float ReadCalibrationFromFlash(const uint32_t *adr_pg, uint16_t space);

#endif
