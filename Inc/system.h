#ifndef __SYSTEM_H
#define __SYSTEM_H


#include "arm_math.h"


#define AUDIO_WEIGHTING_CURVE			(parameters&(uint16_t)0x0001)			/*	'0'	-	A				'1'	-	C				*/		
#define TIME_WEIGHTING_CURVE			(parameters&(uint16_t)0x0002)			/*	'0'	-	FAST		'1'	-	SLOW		*/
#define CLEAR_MIN_MAX_1000MS			(parameters&(uint16_t)0x0004)			/*	'0'	-					'1'	-	CLR			*/
#define CLEAR_MIN_MAX_AVERAGE			(parameters&(uint16_t)0x0008)			/*	'0'	-					'1'	-	CLR			*/
#define RTC_SHOW_TIME_AVERAGE			(parameters&(uint16_t)0x0010)			/*	'0'	-					'1'	-	SHOW		*/
#define RTC_RST_TIME_AVERAGE			(parameters&(uint16_t)0x0020)			/*	'0'	-					'1'	-	RST			*/
#define WAIT_UNTIL_CLR_AVERAGE    !(parameters&(uint16_t)0x0040)
#define SET_HOURS_TIME_AVERAGE    (parameters&(uint16_t)0x0080)
#define SET_MINUTES_TIME_AVERAGE  (parameters&(uint16_t)0x0100)
#define SET_SECONDS_TIME_AVERAGE  (parameters&(uint16_t)0x0200)
#define DISPLAY_LIST_RESULTS_1_2  (parameters&(uint16_t)0x0400)
#define DISPLAY_TIME_AVERAGE		  (parameters&(uint16_t)0x0800)
#define AVERAGE_TIME_REACHED		  (parameters&(uint16_t)0x1000)
#define AVERAGING_IS_ACTIVE				(parameters&(uint16_t)0x2000)
#define TYPE_OF_CALIBRATION_DIS		(parameters&(uint16_t)0x4000)
#define CHOOSEN_USER_CALIBRATION  (parameters&(uint16_t)0x8000)


extern uint32_t language;
extern uint32_t backlit;
extern uint8_t menuposition;
extern uint8_t menuposition_mem;
extern uint16_t parameters;
extern uint32_t total_turn_on;
extern uint32_t time_one_on;
extern uint32_t time_to_off;
extern uint16_t bat_time;

extern void battery_check(uint8_t menu, uint8_t now);
extern void ResetTimeOff(void);
extern void AutoTurnOff(uint8_t now, uint8_t lowbat);
extern void AutoBacklightOff(void);
extern void ConvertSecondsToHMS(uint32_t time_s);
extern void ResetRtc(void);
extern uint32_t GetSecondsRtc(void);






#endif
