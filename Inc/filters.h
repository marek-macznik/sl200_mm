#ifndef __FILTERS_H
#define __FILTERS_H

#include "arm_math.h"

void FiltersInit(void);



extern volatile float artm_db_result_125ms;  //fast
extern volatile float artm_db_result_1000ms; //slow
extern volatile float artm_db_result_inf;    //inf
extern int32_t                      RecBuff[2048];
extern volatile float artm_db_result_1000ms_2; //slow
extern volatile float artm_db_result_125ms_2;  //fast
extern float calibration_value2;
extern float calibration_value3;
extern volatile float rmsa4;
float Integral_Trap(float *input, float sample_rate, uint32_t blockSize);
//extern volatile float test_db_result;
//extern volatile float db_result;





#endif
