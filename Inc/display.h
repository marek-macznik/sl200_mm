#ifndef __DISPLAY_H
#define __DISPLAY_H

#include "arm_math.h"



extern volatile int8_t hour_time_aver;
extern volatile int8_t min_time_aver;
extern volatile int8_t sec_time_aver;
extern volatile uint32_t sumsec_time_aver;

extern float min_val_aver;
extern float max_val_aver;
extern float min_val;   
extern float max_val;

extern uint32_t language;
extern uint32_t backlit;
extern void RTC_AlarmConfig(void);
extern void RTC_Reset_time_buttons(void);

extern void display_results(void);
extern void DisplayFinishedTimeAverage(void);
extern void RTC_Reset_time(void);
extern void DisplayBar(uint8_t menu);
extern void DisplayShortMessage(void);
extern const char* language_labels[3][29];
extern void RTC_TimeShow(void);



#endif
