#ifndef __buttons_H
#define __buttons_H


#include "stm32l4xx_hal.h"
#include "delay.h"

//optional includes
//#include "gui.h"
//#include "labels.h"

/******btn's mcu pins******/
#define BTN_LEFT_PORT					GPIOC
#define BTN_LEFT_PIN 					GPIO_PIN_9
#define BTN_RIGHT_PORT				GPIOC
#define BTN_RIGHT_PIN					GPIO_PIN_6
#define BTN_DOWN_PORT					GPIOC
#define BTN_DOWN_PIN 					GPIO_PIN_7
#define BTN_UP_PORT						GPIOC
#define BTN_UP_PIN						GPIO_PIN_5

/******gpio pull-up, btn shorts to ground******/
#define BTN_LEFT_PRESSED			(!(BTN_LEFT_PORT->IDR & BTN_LEFT_PIN))
#define BTN_LEFT_UNPRESSED		((BTN_LEFT_PORT->IDR & BTN_LEFT_PIN))
#define BTN_RIGHT_PRESSED			(!(BTN_RIGHT_PORT->IDR & BTN_RIGHT_PIN))
#define BTN_DOWN_PRESSED			((BTN_DOWN_PORT->IDR & BTN_DOWN_PIN))
#define BTN_DOWN_UNPRESSED		(!(BTN_DOWN_PORT->IDR & BTN_DOWN_PIN))
#define BTN_UP_PRESSED				(!(BTN_UP_PORT->IDR & BTN_UP_PIN))

/******btn's status flags******/
#define LONG_PRESS_FLAG 			(uint8_t)0x04
#define SHORT_PRESS_FLAG			(uint8_t)0x02
#define IS_PRESSED_FLAG				(uint8_t)0x01

/******index in status table******/
#define BTN_LEFT_INDEX 0
#define BTN_RIGHT_INDEX 1
#define BTN_UP_INDEX 2
#define BTN_DOWN_INDEX 3

/******time in ms for long press******/
#define LONG_PRESS_CNT 400

#define NUM_OF_BTNS 4

extern uint16_t free_space_results;
extern uint16_t free_space_backlit;
extern uint32_t time_in_seconds;
extern uint8_t days;



void Button_LongPress(void);
void Button_IrqEvent(void);
void ButtonFunc_Exe(void);
void SetButtonsFunc(uint8_t screen);

void BL_MenuScreen(void);
void BR_MenuScreen(void);
void BU_MenuScreen(void);
void BD_MenuScreen(void);

void BL_AverageScreen(void);
void BR_AverageScreen(void);
void BD_AverageScreen(void);

void BL_ResetScreen(void);
void BR_ResetScreen(void);
void BD_ResetScreen(void);

void BL_OffScreen(void);
void BR_OffScreen(void);
void BD_OffScreen(void);

void BL_MainOffScreen(void);
void BR_MainOffScreen(void);
void BD_MainOffScreen(void);

void BL_SetTimeScreen(void);
void BR_SetTimeScreen(void);
void BD_SetTimeScreen(void);

void BL_CalibrationScreen(void);
void BR_CalibrationScreen(void);
void BD_CalibrationScreen(void);

void BL_MenuPos1(void);
void BR_MenuPos1(void);
void BD_MenuPos1(void);

void BL_MenuPos2(void);
void BR_MenuPos2(void);
void BD_MenuPos2(void);

void BL_MenuPos3(void);
void BR_MenuPos3(void);
void BD_MenuPos3(void);

void BL_MenuPos4(void);
void BR_MenuPos4(void);
void BD_MenuPos4(void);

void BL_MenuPos5(void);
void BR_MenuPos5(void);
void BD_MenuPos5(void);

void BL_MenuPos6(void);
void BR_MenuPos6(void);
void BD_MenuPos6(void);

void BL_MenuPos7(void);
void BR_MenuPos7(void);
void BD_MenuPos7(void);

void BL_MenuPos8(void);
void BR_MenuPos8(void);
void BD_MenuPos8(void);

void BL_MenuBacklit(void);
void BR_MenuBacklit(void);
void BD_MenuBacklit(void);

void BL_MenuSetTimeAverage(void);
void BR_MenuSetTimeAverage(void);
void BD_MenuSetTimeAverage(void);

void BL_MenuDisplayResults(void);
void BR_MenuDisplayResults(void);
void BD_MenuDisplayResults(void);

void BL_MenuSetLanguage(void);
void BR_MenuSetLanguage(void);
void BD_MenuSetLanguage(void);

void BL_MenuUserCalibration(void);
void BR_MenuUserCalibration(void);
void BD_MenuUserCalibration(void);

void BL_MenuSystemInfo(void);
void BR_MenuSystemInfo(void);
void BD_MenuSystemInfo(void);

void BL_MenuDefaultSettings(void);
void BR_MenuDefaultSettings(void);
void BD_MenuDefaultSettings(void);

void BL_MenuWarningFilters(void);
void BR_MenuWarningFilters(void);
void BD_MenuWarningFilters(void);

void BL_MenuSelectCalibration(void);
void BR_MenuSelectCalibration(void);
void BD_MenuSelectCalibration(void);

void BL_BacklightOff(void);
void BR_BacklightOff(void);
void BD_BacklightOff(void);




#endif
